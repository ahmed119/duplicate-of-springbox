import React, { useEffect, useState } from "react";
import Image from "next/image";

import InputMailDownload from "../components/inputMailDownload";

import ColorGuide from "../styles/colorGuide";

import useWindowSize from "../hooks/useWindowSize";

export default function FollowNewsletter(props) {
  const {} = props;
  const { isMobileLayout } = useWindowSize();
  
  return (
    <div className="full_bloc">
      <div className="container">
        <div
          className="containerCenter"
          style={{
            flexDirection: "column",
            textAlign: "center",
            backgroundColor: ColorGuide.lightBlue,
            borderRadius: 20,
            padding: "5%",
          }}
        >
          <h2 style={{ maxWidth: 800 }}>
            Learn <span className="underlineBlue">something new</span> about
            investing every week. Sign up to our Springbox AI emails.
          </h2>

          <InputMailDownload isSignup={true} />
        </div>
      </div>
    </div>
  );
}
