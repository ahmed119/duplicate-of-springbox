import { useRouter } from "next/router";

import Link from "next/link";

import useWindowSize from "../hooks/useWindowSize";

import ColorGuide from "../styles/colorGuide";

import styles from "./footer.module.css";

import AppFooter from "./appFooter";

import premiumBenefits from "../data/premiumBenefitsList";

export default function Footer() {
  const { isMobileLayout, isTabletPortraitLayout } = useWindowSize();
  const { route } = useRouter();

  const isColumnFooter = isMobileLayout || isTabletPortraitLayout;

  const socialList = [
    {
      source: "/social/twitter.png",
      link: "https://twitter.com/Springbox_Ai",
    },
    {
      source: "/social/facebook.png",
      link: "https://www.facebook.com/springboxAI",
    },
    {
      source: "/social/instagram.png",
      link: "https://www.instagram.com/springboxai/",
    },
    {
      source: "/social/discord.png",
      link: "https://www.instagram.com/springboxai/",
    },
  ];

  const footerMenuList = [
    {
      title: "Learn More",
      color: ColorGuide.mainYellow,
      route: "/pricing#FAQ",
    },
    { title: "Our Story", color: ColorGuide.mainBlue, route: "/story" },
    {
      title: "Privacy Policy",
      color: ColorGuide.mainWhite,
      route: "/privacy",
    },
    {
      title: "Terms and conditions",
      color: ColorGuide.mainWhite,
      route: "/terms",
    },
    {
      title: "Legal",
      color: ColorGuide.mainWhite,
      route: "/legal",
    },
    {
      title: "Contact",
      color: ColorGuide.mainWhite,
      route: "mailto:info@springbox.tech",
    },
  ];

  return (
    <div id="footer">
      <AppFooter
        title={"Get Started with your Free 14-Day Trial of Springbox AI."}
        tagLine={`Download the App now and join Springbox AI Available for both iOS and Android.`}
        mockupSource={"/mockups/homeWebMobile.png"}
      >
        <>
          <p>
            <span>
              Subscribe to unlock the full Springbox AI licence and start making
              profits.
            </span>
            <br />
            <br />
            <span>
              First 14 Days Free. Unlimited Access.
              <br />
              Cancel Anytime.
            </span>
            <br />
            <br />
          </p>

          <div style={{ marginBottom: 30 }}>
            {premiumBenefits.map((benefit) => (
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-start",
                  paddingTop: isMobileLayout ? 5 : 0,
                  paddingBottom: isMobileLayout ? 5 : 0,
                }}
              >
                <img
                  style={{
                    height: 15,
                    marginRight: 10,
                    marginTop: 3,
                  }}
                  src="/UI/arrowThinWhite.png"
                />
                <p style={{ fontWeight: "600" }}>{benefit}</p>
              </div>
            ))}
          </div>
        </>
      </AppFooter>

      <footer style={{ backgroundColor: ColorGuide.mainBlack }}>
        <div className="container">
          <div
            id={styles.containerFooterActions}
            className="containerSpaceBetween"
            style={{ marginBottom: 80 }}
          >
            <div id={styles.containerSocialItems} style={{ width: "30%" }}>
              <img
                style={{ width: 300, marginBottom: 20 }}
                src="/springbox-logo-white.png"
              />

              <div className="containerRowMobile">
                {socialList.map(({ source, link }, index) => {
                  const iconSize = isMobileLayout ? 25 : 35;

                  return (
                    <Link href={link}>
                      <div
                        key={index}
                        style={{
                          display: "flex",
                          backgroundColor: ColorGuide.mainOrange,
                          width: iconSize,
                          height: iconSize,
                          marginRight: index !== socialList.length - 1 && 15,
                          justifyContent: "center",
                          alignItems: "center",
                          borderRadius: 5,
                        }}
                      >
                        <img style={{ width: "60%" }} src={source} />
                      </div>
                    </Link>
                  );
                })}
              </div>
            </div>

            <div
              className="halfBloc"
              style={{ width: isColumnFooter && "100%" }}
            >
              {footerMenuList.map(({ title, color, route }, index) => (
                <Link key={title} href={route}>
                  <button
                    style={{
                      borderColor: color,
                      display: "initial",
                      marginRight: 10,
                      marginBottom: 10,
                    }}
                  >
                    <p style={{ color }}>{title}</p>
                  </button>
                </Link>
              ))}
            </div>
          </div>

          <p style={{ color: ColorGuide.mainGrey }}>
            Springbox Ai. LTD (“Springbox AI”) does not provide investment, tax
            or legal advice and does not endorse or recommend the purchase or
            sale of any particular security or trading strategy.
            <br />
            <br />
            Certain features of the Springbox AI video, website (“Website”) and
            the service (“Service”) include charts and interactive tools
            designed to generate and display amongst other things: (a) past
            performance data; (b) future price and outcome expectations derived
            from markets data; and, (c) potential stock and trading forecasting
            and strategies based upon your personal inputs. These features and
            all of their content are meant for informational and reference
            purposes only and are not intended to serve as a recommendation to
            buy or sell any security or to utilize any particular trading
            strategy. They are also not research reports, do not reflect the
            opinions or expectations of Springbox AI and should not be
            considered as a basis for judging future trends or as an indication
            of future results. Any data or information regarding the likelihood
            of various investment outcomes is hypothetical in nature, is not
            guaranteed for accuracy or completeness, does not reflect actual
            investment results and is not a guarantee of future results.
            <br />
            <br />
            The Website and the Video may include certain features provided
            solely to demonstrate tools and other Content that are part of the
            Service available only to Customers of Springbox AI. Demo features
            are intended for informational purposes only and should not be used
            as the basis for making investment decisions or for any other
            purpose. Demo features use delayed or static data and Springbox AI
            makes no warranty whatsoever as to their accuracy or usefulness.
            <br />
            <br />
            You alone are responsible for evaluating the merits and risks
            associated with use of the Video the Website and the Service and for
            determining whether its features and tools are suitable for you. You
            are also solely responsible for your personal inputs, for evaluating
            all information and for any investment decision you make, whether or
            not such decision derives from your use of the Service. Please
            review our full Terms and Conditions.
            <br />
            <br />
            All investments involve risk and all investors should consider their
            investment objectives and risks carefully before investing. The
            price of a given security may increase or decrease based on market
            conditions and customers may lose money, including their original
            investment. Any investment or trade made using the Springbox AI
            Service is at your sole discretion and risk.
            <br />
            <br />
            This is not an offer, or advice to buy or sell securities in any
            jurisdiction where Springbox AI is not registered or where such
            products and other services offered by the firm would be contrary to
            the securities regulations of that jurisdiction.
            <br />
            <br />
            Online trading involves risks due to loss of online services, system
            response time, execution price, trading volumes, market conditions,
            erroneous or unavailable market data and other factors.
          </p>
        </div>
      </footer>
    </div>
  );
}
