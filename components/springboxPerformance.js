import React from "react";
import { RichText, Date } from "prismic-reactjs";
import { default as NextLink } from "next/link";

import useWindowSize from "../hooks/useWindowSize";

import ColorGuide from "../styles/colorGuide";

const SpringboxPerformance = (params) => {
  const { isPortraitLayout } = useWindowSize();

  return (
    <div
      className="full_bloc"
      style={{ backgroundColor: ColorGuide.mainGreen }}
    >
      <div className="container">
        <h2
          style={{
            textAlign: "center",
            color: "white",
            width: isPortraitLayout ? "100%" : "70%",
            margin: "0 auto",
            marginBottom: 30,
          }}
        >
          Cumulative profits 2021 if all Springbox AI recommendations were
          followed:
        </h2>

        <h1 style={{ color: ColorGuide.mainOrange, textAlign: "center" }}>
          +1185.48%<span style={{ color: ColorGuide.mainWhite }}>*</span>
        </h1>

        <h4
          style={{
            textAlign: "center",
            color: ColorGuide.mainWhite,
            marginBottom: 40,
          }}
        >
          As per 07/30/2021
        </h4>

        <p style={{ textAlign: "center", color: ColorGuide.mediumGrey }}>
          *Any data or information regarding the likelihood of various
          investment outcomes is hypothetical in nature, is not guaranteed for
          accuracy or completeness, does not reflect actual investment results
          and is not a guarantee of future results.
        </p>
      </div>
    </div>
  );
};

export default SpringboxPerformance;
