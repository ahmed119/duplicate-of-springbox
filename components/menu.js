import { useState, useEffect } from "react";

import { useRouter } from "next/router";

import Link from "next/link";

import useWindowSize from "../hooks/useWindowSize";

import menuRoutesList from "../data/menuRoutesList";
import socialList from "../data/socialList";

import ColorGuide from "../styles/colorGuide";

import styles from "./menu.module.css";

export default function Menu() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const {
    width: responsiveWidth,
    height: responsiveHeight,
    isMobileLayout,
  } = useWindowSize();
  const { route, push } = useRouter();

  const menuBreakPoint = responsiveWidth < 1020;

    const handleClick = (event) => {
    if (typeof window !== 'undefined'){
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({ event:'click_get_started' });
    }
  };
  const RenderButtonTrial = ({ text, style, textStyle }) => (
    <Link href="/pricing">
      <div
        style={{ cursor: "pointer", ...style }}
        className={`${styles.containerActionMenu} shadowLightBlue`}
      >
        <p
          style={{
            color: ColorGuide.mainBlue,
            fontWeight: "600",
            marginRight: 5,
            ...textStyle,
          }}
        >
          {text || "Get Started"}
        </p>

        <img src="/UI/chevronBlue.png" alt="start trading" />
      </div>
    </Link>
  );

  return (
    <>
      <div id={styles.main_menu}>
        <div className={styles.content_menu}>
          <div className="containerRow" style={{ alignItems: "center" }}>
            <Link href="/">
              <img
                onClick={() => setIsMenuOpen(false),handleClick()}
                style={{
                  width: responsiveWidth < 650 && "40vw",
                  cursor: "pointer",
                }}
                id={styles.logo}
                src="/springbox-logo.png"
                alt="springbox Logo"
              />
            </Link>

            {!menuBreakPoint && (
              <ul>
                {menuRoutesList
                  .filter(({ onlyMobile = false }) => !onlyMobile)
                  .map((page, index) => (
                    <li key={index}>
                      <Link href={page.route}>
                        <p style={{ fontWeight: "600" }}>{page.title}</p>
                      </Link>
                    </li>
                  ))}
              </ul>
            )}
          </div>

          {menuBreakPoint ? (
            <img
              onClick={() => setIsMenuOpen(!isMenuOpen)}
              style={{ width: 20 }}
              src="/UI/burgerMenu.png"
              alt="start trading"
            />
          ) : (
            <RenderButtonTrial />
          )}
        </div>
      </div>

      {isMenuOpen && (
        <div
          style={{
            flexDirection: "column",
            alignItems: "flex-start",
            zIndex: 100,
            position: "fixed",
            top: 0,
            right: 0,
            left: 0,
            bottom: 0,
            width: "100%",
            height: "100%",
            paddingTop: 50,
            backgroundColor: ColorGuide.lightBlue,
          }}
        >
          <div className="container">
            {menuRoutesList.map(({ title, route }, index) => (
              <h2
                onClick={() => {
                  setIsMenuOpen(false);
                  push(route);
                }}
                style={{
                  fontWeight: "700",
                  marginBottom: index == menuRoutesList.length - 1 ? 30 : 20,
                }}
              >
                {title}
              </h2>
            ))}

            <div style={{ marginBottom: 30 }}>
              <p style={{ marginBottom: 10 }}>Connect:</p>

              {socialList.map(({ title, icon, link }) => (
                <div className="containerRowMobile">
                  <div
                    className="containerCenter"
                    style={{
                      width: 20,
                      height: 20,
                      backgroundColor: ColorGuide.lightOrange,
                      padding: 10,
                      margin: 10,
                      borderRadius: 10,
                    }}
                  >
                    <img
                      style={{ width: "70%" }}
                      src={icon}
                      alt="start trading"
                    />
                  </div>

                  <p>{title}</p>
                </div>
              ))}
            </div>

            <RenderButtonTrial
              text="Get 14 days free"
              style={{ width: "60%", padding: 15 }}
              textStyle={{ fontSize: 18, fontWeight: "600" }}
            />
          </div>
        </div>
      )}
    </>
  );
}
