import React, { useEffect, useState } from "react";
import Image from "next/image";

import useWindowSize from "../hooks/useWindowSize";

export default function SmartImage(props) {
  const {
    src,
    onClick,

    className,
    style,
    loading = "lazy",

    layout = "responsive",
    width,
    height,
    objectFit,

    dataRellaxspeed,
    alt = "Agence Minuit",
  } = props;
  const { responsiveWidth, responsiveHeight } = useWindowSize();

  return (
    <div
      onClick={onClick}
      className={className}
      style={style}
      data-rellax-speed={dataRellaxspeed}
    >
      <Image
        alt={alt}
        src={src}
        quality={width > 700 ? 75 : 85}
        width={width}
        height={height}
        layout={layout}
        loading={loading}
        objectFit={objectFit}
      />
    </div>
  );
}
