import React, { useState } from "react";

import Link from "next/link";
import { useRouter } from "next/router";
import moment from "moment";

import ColorGuide from "../styles/colorGuide";

import { downloadLinkList } from "../data/mainConfig";

import useWindowSize from "../hooks/useWindowSize";

import styles from "./downloadApp.module.css";

export default function ArticlePreview({
  articleID,
  summaryList = [],
  title,
  publicationDate = new Date(),
  readTime = 0,
  thumbnail = null,

  isPreview = true,
  useRowLayout = false,
  reverse = false,
}) {
  const { isTabletPortraitLayout, isMobileLayout } = useWindowSize();

  const { push } = useRouter();

  const showFullScreenArticle = isTabletPortraitLayout || isMobileLayout;

  const goToArticle = () =>
    push({
      pathname: "/articleDetails",
      query: { articleID },
    });

  return (
    <div
      style={
        showFullScreenArticle
          ? { display: "flex", flexDirection: "column", marginBottom: 50 }
          : useRowLayout
          ? {
              display: "flex",
              flexDirection: reverse ? "row-reverse" : "row",
              alignItems: "center",
            }
          : {}
      }
    >
      <div style={{ position: "relative", marginBottom: 30 }}>
        <img
          className="shadowBlack"
          style={{
            width:
              useRowLayout && !reverse && !showFullScreenArticle
                ? "90%"
                : "100%",
            borderRadius: 40,
          }}
          src={thumbnail}
          alt="trade dashboard"
        />

        <div
          style={{
            position: "absolute",
            top: 25,
            left: 25,
            backgroundColor: "rgba(255,255,255,0.8",
            padding: 10,
            paddingRight: 20,
            paddingLeft: 20,
            borderRadius: 50,
          }}
        >
          <p style={{ color: ColorGuide.mainBlue, fontWeight: "600" }}>
            Article
          </p>
        </div>
      </div>

      <div
        style={{
          width: useRowLayout && !showFullScreenArticle ? "150%" : "100%",
          marginRight: !showFullScreenArticle && reverse ? 40 : 0,
        }}
      >
        <h4>{moment(publicationDate).format("DD MMMM YYYY")}</h4>
        <h2
          onClick={goToArticle}
          style={{ cursor: "pointer", marginBottom: 30 }}
        >
          {title}
        </h2>

        <p style={{ marginBottom: 30 }}>{summaryList[0] || ""}</p>

        {isPreview ? (
          <p
            onClick={goToArticle}
            style={{
              fontWeight: "600",
              cursor: "pointer",
              color: ColorGuide.mainOrange,
              textDecoration: "underline",
            }}
          >
            Read article
          </p>
        ) : (
          <div className="containerRow">
            <img
              style={{
                width: 20,
                marginRight: 10,
              }}
              src="/UI/read.png"
              alt="trade dashboard"
            />

            <p
              style={{ color: ColorGuide.mainBlue, fontWeight: "600" }}
            >{`${readTime} minutes read`}</p>
          </div>
        )}
      </div>
    </div>
  );
}
