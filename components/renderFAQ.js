import React, { useState } from "react";
import { RichText, Date } from "prismic-reactjs";
import { default as NextLink } from "next/link";

import useWindowSize from "../hooks/useWindowSize";

import ColorGuide from "../styles/colorGuide";

const RenderFAQ = ({ dataFAQ }) => {
  const { isMobileLayout, width, height } = useWindowSize();

  const [currentFAQIndex, setCurrentFAQIndex] = useState(0);

  return dataFAQ.map(({ title, text }, index) => (
    <>
      <div
        onClick={() =>
          setCurrentFAQIndex(currentFAQIndex === index ? null : index)
        }
        className="containerSpaceBetween"
        style={{ marginBottom: 20, cursor: "pointer" }}
      >
        <h3
          style={{
            width: "90%",
            fontFamily: "Work Sans",
            fontWeight: "700",
            fontSize: isMobileLayout ? 18 : 25,
            margin: 0,
          }}
        >
          {title}
        </h3>

        <img
          style={{
            width: 20,
            transition: "1s",
            transform: currentFAQIndex !== index && "rotate(2700deg)",
          }}
          src={currentFAQIndex === index ? "/UI/minus.png" : "/UI/plus.png"}
        />
      </div>

      {currentFAQIndex === index ? (
        <p style={{ marginBottom: 50 }}>{text}</p>
      ) : (
        <div
          style={{
            width: "100%",
            height: 2,
            backgroundColor: ColorGuide.mainBlue,
            opacity: 0.2,
            marginTop: 20,
            marginBottom: 20,
          }}
        />
      )}
    </>
  ));
};

export default RenderFAQ;
