import React from "react";
import { RichText } from "prismic-reactjs";

/**
 * Component that returns the first paragraph of a post
 */
const FirstParagraph = ({ sliceZone, textLimit = 200, style = {} }) => {
  // Find the first text slice of post's body
  const firstTextSlice = sliceZone.find((slice) => slice.type === "paragraph");

  if (firstTextSlice) {
    let limitedText = firstTextSlice.text.substring(0, textLimit);

    if (limitedText.length > textLimit) {
      // Cut only up to the last word and attach '...' for readability
      limitedText = `${limitedText.substring(
        0,
        limitedText.lastIndexOf(" ")
      )}...`;
    }

    return <p style={style}>{limitedText}</p>;
  }

  // If there are no slices of type 'text', return nothing
  return null;
};

export default FirstParagraph;
