import React, { useState } from "react";

import Link from "next/link";

import ColorGuide from "../styles/colorGuide";

import { downloadLinkList } from "../data/mainConfig";

import useWindowSize from "../hooks/useWindowSize";

import styles from "./downloadApp.module.css";

export default function DownloadApp({
  mainContainerStyle,
  containerStyle,
  containerClass,
  addedClass,
  showStoreButtons = true,
  buttonText = null,
}) {
  const { isMobileLayout } = useWindowSize();
   const handleClick = (event) => {
    window.dataLayer.push({ event:'download_app' });
  };



  return (
    <div style={mainContainerStyle}>
      {!isMobileLayout && showStoreButtons && (
        <div
          className={`containerRowMobile ${addedClass}`}
          style={{ marginBottom: 40 }}
        >
          {downloadLinkList.map(({ source, link }, index) => (
            <Link href={link}>
              <img
                alt="stock trading app"
                style={{ cursor: "pointer", marginRight: index === 0 && 15 }}
                key={index}
                className={styles.iconStoreDownload}
                src={source}
              />
            </Link>
          ))}
        </div>
      )}

      <Link href="https://springbox.page.link/download">
        <button
          onClick={() => handleClick()}
          className={`${containerClass} containerCenter`}
          style={{
            cursor: "pointer",
            backgroundColor: ColorGuide.mainWhite,
            padding: isMobileLayout ? 10 : 15,
            width: isMobileLayout ? 230 : 250,
            borderWidth: 0,
            ...containerStyle,
          }}
        >
          <p
            style={{
              color: ColorGuide.mainBlue,
              fontWeight: "600",
              marginRight: 10,
            }}
          >
            {buttonText || "Download the App"}
          </p>

          <img
            src="/UI/chevronBlue.png"
            style={{ width: 10 }}
            alt="start trading"
          />
        </button>
      </Link>
    </div>
  );
}
