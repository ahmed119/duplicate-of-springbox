import React, { useState } from "react";

import Link from "next/link";
import QRCode from "react-qr-code";

import ColorGuide from "../styles/colorGuide";

import useWindowSize from "../hooks/useWindowSize";

import styles from "./footer.module.css";

export default function TeamBubbles() {
  const [currentTeamMember, setCurrentTeamMember] = useState(null);

  const { width, height } = useWindowSize();

  const isMobile = width < 650;

  const defaultPadding = width < 650 ? 30 : 50;

  const teamData = [
    {
      name: "Logan Lagarre Jumeau",
      picture: "/team/fayze.png",
      linkedin: "https://www.linkedin.com/in/fayzebouaouid/",
      style: {
        backgroundColor: ColorGuide.mainBlack,
        width: 90,
        height: 90,
      },
      textColor: ColorGuide.mainOrange,
      position: { top: "31%", right: "30%" },
      positionMobile: { top: "15%", right: "27%" },
    },
    {
      isCofounder: true,
      role: "Partner - Chief Strategy Officer",
      name: "Kassem Lahham",
      picture: "/team/kassem.png",
      linkedin: "https://www.linkedin.com/in/kassemlahham/",
      description:
        "Kassem is the Co-Founder and Chief Strategist Advisor of financial intelligence application Springbox AI. His career path spans over more than three decades in the financial industry. A financial architect with in-depth knowledge and experience, Kassem takes a very goal-oriented approach to passing on advice and assistance. Kassem is a successful FinTech entrepreneur and regular speaker at financial conferences, and has forged a path that allows him the freedom to pass on his banking and finance knowledge to a new generation of investors.",
      style: {
        backgroundColor: ColorGuide.mainYellow,
        width: 130,
        height: 130,
      },
      textColor: ColorGuide.mainBlue,
      position: { bottom: "39%", right: "17%" },
      positionMobile: { bottom: "41%", right: "12%" },
    },
    {
      isCofounder: true,
      role: "Partner - Chief AI Strategist",
      name: "Mostafa Belkhayate",
      picture: "/team/mostafa.png",
      linkedin: "https://www.linkedin.com/in/belkhayate/",
      description:
        "Mostafa is the Co-Founder and Chief AI Strategist of financial intelligence application Springbox AI. Fund Manager, Mostafa brings his experience in trading and his knowledge of AI systems as applied to financial investing to Springbox AI, which utilizes machine learning to help users make smart and fast financial decisions. One of Mostafa’s funds was ranked in 2006-2007 by Bloomberg as the best Fund in its category, earning him the best Gold Manager of the year. In addition, he was the winner of the 2009 Gold Trophy of Technical Analysis, and in 2010 of the Silver Trophy for the same category.",
      style: {
        backgroundColor: ColorGuide.lightBlue,
        width: 130,
        height: 130,
      },
      textColor: ColorGuide.mainBlue,
      position: { bottom: "24%", right: "31%" },
      positionMobile: { bottom: "15%", right: "22%" },
    },
    {
      isCofounder: true,
      role: "Partner - CEO",
      name: "Fayze Bouaouid",
      picture: "/team/fayze.png",
      linkedin: "https://www.linkedin.com/in/fayzebouaouid/",
      description:
        "Fayze is the Co-Founder and CEO of financial intelligence application Springbox AI. With nearly two decades in the banking and asset management industries, Fayze has spent his career learning every detail of financial markets, banking and investing. He brings his wealth of asset management knowledge to Springbox AI, in the service of assisting young investors find success. Fayze is working toward a democratized future of investing for everyone.",
      style: { backgroundColor: ColorGuide.mainBlue, width: 130, height: 130 },
      textColor: "white",
      position: { top: "33%", left: "27%" },
      positionMobile: { bottom: "24%", left: "25%" },
    },
    {
      role: "Sr. Software Engineer",
      name: "Theo Hudry",
      picture: "/team/theo.png",
      linkedin: "https://www.linkedin.com/in/théo-hudry-34410549/",
      description:
        "Theo has been developing mobile apps with his team for five years, he loves clean interfaces and state of the art high performance software that outperform expectations.",
      style: { backgroundColor: ColorGuide.mainGreen, width: 100, height: 100 },
      textColor: ColorGuide.mainYellow,
      position: { bottom: "44%", right: "39%" },
      positionMobile: { bottom: "49%", left: "40%" },
    },
    {
      role: "Sr. ML Engineer",
      name: "Regis Nebor",
      picture: "/team/regis.png",
      linkedin: "https://www.linkedin.com/in/rnebor/",
      description:
        "Regis has been dedicating his analytical skills to the financial markets for almost 13 years helping individuals and money managers capture profits through carefully orchestrated automation.",
      style: { backgroundColor: ColorGuide.mainYellow, width: 90, height: 90 },
      textColor: ColorGuide.mainOrange,
      position: { bottom: "34%", left: "37%" },
      positionMobile: { bottom: "51%", left: "20%" },
    },
    {
      name: "Thomas Demirdjian",
      picture: "/team/fayze.png",
      linkedin: "https://www.linkedin.com/in/fayzebouaouid/",
      style: { backgroundColor: ColorGuide.lightBlue, width: 65, height: 65 },
      textColor: ColorGuide.mainBlue,
      position: { top: "30%", left: "46%" },
      positionMobile: { top: "47%", left: "12%" },
    },
  ];

  return (
    <div
      className="full_bloc"
      style={{
        background: width > 650 && "rgb(255,255,255)",
        background:
          width > 650 &&
          "linear-gradient(90deg, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 50%, rgba(255,145,110,1) 50%, rgba(255,145,110,1) 100%)",
      }}
    >
      <div
        id="teamDescription"
        className={`container containerRow fullWidthMobile`}
        style={{ padding: 0 }}
      >
        <div
          style={{
            position: "relative",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
          }}
          className={`halfBloc alternateBloc containerMobile`}
        >
          {!currentTeamMember ? (
            <>
              <h4 style={{ marginBottom: 25 }}>MEET THE TEAM</h4>
              <h1 style={{ color: ColorGuide.mainBlue, marginBottom: 40 }}>
                We worked together with a highly-specialised group of scientists
                and researchers.
              </h1>
              <p>
                From the fields of artificial intelligence, mathematics,
                quantitative trading, and computer science, they came up with a
                revolutionary approach, breaking with the past and aiming for
                the future.
                <br />
                <br />
                Because we believe that the future of trading is here, it’s
                data-driven and more inclusive than ever before.
              </p>
            </>
          ) : (
            <div
              style={{
                zIndex: 1200,
                position: "relative",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                borderRadius: 20,
                padding: width < 650 ? 35 : 50,
                backgroundColor: ColorGuide.lightBlue,
              }}
            >
              <img
                onClick={() => setCurrentTeamMember(null)}
                style={{
                  cursor: "pointer",
                  position: "absolute",
                  top: 30,
                  right: 30,
                  width: 30,
                  height: 30,
                }}
                src="/UI/close.png"
              />

              <div
                style={{
                  position: "relative",
                  width: 130,
                  height: 130,
                  marginBottom: 30,
                }}
              >
                <img
                  style={{
                    width: "100%",
                    height: "100%",
                    borderRadius: "50%",
                  }}
                  src={teamData[currentTeamMember].picture}
                />

                {teamData[currentTeamMember].isCofounder && (
                  <Link href={teamData[currentTeamMember].linkedin}>
                    <div
                      className="containerCenter"
                      style={{
                        cursor: "pointer",
                        position: "absolute",
                        bottom: -10,
                        right: -10,
                        height: 50,
                        width: 50,
                        backgroundColor: ColorGuide.mainOrange,
                        borderRadius: "50%",
                      }}
                    >
                      <img
                        style={{
                          width: "40%",
                          height: "40%",
                        }}
                        src="/social/linkedin.png"
                      />
                    </div>
                  </Link>
                )}
              </div>

              <h3
                style={{
                  fontSize: width > 650 && 20,
                  color: ColorGuide.mainBlue,
                }}
              >
                Meet {teamData[currentTeamMember].name}
              </h3>
              {teamData[currentTeamMember].role && (
                <h3
                  style={{
                    fontFamily: "Work Sans",
                    fontWeight: "600",
                    fontSize: width > 650 && 20,
                    color: ColorGuide.mainOrange,
                    marginBottom: 20,
                  }}
                >
                  {teamData[currentTeamMember].role}
                </h3>
              )}

              <p
                className="small"
                style={{ color: ColorGuide.mainBlue, width: "90%" }}
              >
                {teamData[currentTeamMember].description}
              </p>
            </div>
          )}
        </div>

        <div
          className="halfBloc containerCenter"
          style={{ backgroundColor: width < 650 && ColorGuide.mainOrange }}
        >
          <div
            style={{
              position: "relative",
              minWidth: width < 650 ? 500 : 700,
              height: width < 650 ? 500 : 700,
            }}
          >
            {teamData.map(
              (
                {
                  isCofounder,
                  name,
                  description,
                  picture,
                  position,
                  positionMobile,
                  style,
                  textColor,
                },
                index
              ) => (
                <div
                  onClick={() => {
                    if (description) {
                      setCurrentTeamMember(index);

                      if (width < 650) {
                        const element = document.querySelector(
                          "#teamDescription"
                        );

                        if (element) {
                          // Smooth scroll to that elment
                          element.scrollIntoView({
                            behavior: "smooth",
                            block: "start",
                            inline: "nearest",
                          });
                        }
                      }
                    }
                  }}
                  className="containerCenter"
                  style={{
                    transition: ".3s",
                    ...style,
                    position: "absolute",
                    ...(width > 650 ? position : positionMobile),
                    borderRadius: 500,
                    cursor: "pointer",
                    ...(currentTeamMember === index
                      ? {
                          boxSizing: "border-box",
                          borderWidth: 5,
                          borderColor: ColorGuide.mainWhite,
                          borderStyle: "solid",
                        }
                      : {}),
                  }}
                >
                  <img
                    style={{
                      transform: "rotate(90deg)",
                      width: style.width * 0.15,
                      height: style.width * 0.15,
                      marginRight: style.width * 0.05,
                    }}
                    src="/UI/arrowOrange.png"
                  />
                  <h3
                    style={{ color: textColor, fontSize: style.width * 0.15 }}
                  >
                    {name
                      .split(" ")
                      .map((n) => n[0])
                      .join("")}
                  </h3>
                </div>
              )
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
