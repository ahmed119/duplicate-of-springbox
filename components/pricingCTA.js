import Link from "next/link";
import { useState } from "react";

import DownloadApp from "../components/downloadApp";

import useWindowSize from "../hooks/useWindowSize";

import ColorGuide from "../styles/colorGuide";

import styles from "./appFooter.module.css";

import { downloadLinkList } from "../data/mainConfig";
import premiumBenefits from "../data/premiumBenefitsList";

export default function PricingCTA({
  style,
  title,
  tagLine,
  showPrice = true,
  showStoreButtons = true,
  children,
  buttonColor,
  mockupSource,
  mockupStyle,
  reverseLayout = false,
}) {




  const [userMail, setUserMail] = useState(null);
  const [mailStatus, setMailStatus] = useState(null);

  const { isMobileLayout } = useWindowSize();
  

  const checkIfEmailIsValid = (mail) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(mail).toLowerCase());
  };
  
    const handleClick = (event) => {
    if (typeof window !== 'undefined'){
    window.dataLayer = window.dataLayer || [];
    }
    window.dataLayer.push({ event:'click_14_days' });
  };
  const FooterTitle = () => (
    <>
      <h4>GET STARTED</h4>

      <h2
        style={{
          fontSize: isMobileLayout && 23,
          color: ColorGuide.mainBlack,
          marginBottom: 50,
        }}
      >
        <span className="underlineOrange">Free trial</span>
      </h2>
    </>
  );

  return (
    <div
      className="full_bloc"
      style={{
        position: "relative",
        backgroundColor: ColorGuide.mainWhite,
        overflow: "hidden",
        ...style,
      }}
    >
      <div className="container containerRow">
        <div className="halfBloc" style={{ position: "relative" }}>
          {!isMobileLayout && (
            <img
              style={{
                zIndex: -1,
                position: "absolute",
                top: 180,
                left: -120,
                width: 300,
                transform: "rotate(90deg)",
              }}
              src="/art/springboxBubbles.png"
              alt="stock market forecast"
            />
          )}

          <FooterTitle />

          <img
            style={{ width: "100%" }}
            id={styles.mockupWebMobile}
            src={"/mockups/homeWebMobile.png"}
          />
        </div>

        <div className="halfBloc" style={{ position: "relative" }}>
          {!isMobileLayout && (
            <img
              style={{
                zIndex: -1,
                position: "absolute",
                top: -60,
                left: -100,
                width: 300,
                transform: "rotate(180deg)",
              }}
              src="/art/springboxBubbles.png"
              alt="stock market forecast"
            />
          )}

          <h4 style={{ fontWeight: "600", marginBottom: 30 }}>
            PREMIUM Benefits
          </h4>

          <div style={{ marginBottom: 30 }}>
            {premiumBenefits.map((benefit) => (
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "flex-start",
                  marginBottom: isMobileLayout ? 15 : 10,
                }}
              >
                <img
                  style={{
                    height: 15,
                    marginRight: 10,
                    marginTop: 4,
                  }}
                  src="/UI/arrowThinBlue.png"
                />
                <p style={{ fontWeight: "600" }}>{benefit}</p>
              </div>
            ))}
          </div>

          <Link href="/pricing">
            <button
              onClick={() => handleClick}
              className="containerCenter shadowBlack"
              style={{
                cursor: "pointer",
                backgroundColor: ColorGuide.mainOrange,
                padding: 15,
                width: 250,
                borderWidth: 0,
              }}
            >
              <p
                style={{
                  color: ColorGuide.mainBlue,
                  fontWeight: "600",
                  marginRight: 10,
                }}
              >
                Get 14 days free
              </p>

              <img
                style={{ width: 15 }}
                src="/UI/chevronBlue.png"
                alt="start trading"
              />
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
}
