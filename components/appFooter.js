import Link from "next/link";
import { useState } from "react";

import DownloadApp from "../components/downloadApp";

import useWindowSize from "../hooks/useWindowSize";

import ColorGuide from "../styles/colorGuide";

import styles from "./appFooter.module.css";

import { downloadLinkList } from "../data/mainConfig";

export default function AppFooter({
  style,
  title,
  tagLine,
  showPrice = true,
  children,
  buttonColor,
  mockupSource,
  mockupStyle,
}) {
  const [userMail, setUserMail] = useState(null);
  const [mailStatus, setMailStatus] = useState(null);

  const {
    isMobileLayout,
    width: responsiveWidth,
    height: responsiveHeight,
  } = useWindowSize();

  const addToNewsletter = (e) => {
    try {
      e.preventDefault(); // Stops the page from reloading!

      if (!checkIfEmailIsValid(userMail)) {
        throw new Error("Mail not valid");
      }

      splitbee.user.set({ email: userMail });

      setUserMail(null);
      setMailStatus("success");
      console.log("added to newsletter");
    } catch (e) {
      setMailStatus("error");
    } finally {
    }
  };

  const checkIfEmailIsValid = (mail) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(mail).toLowerCase());
  };

  const renderDownloadLinkList = ({ addedClass }) => (
    <DownloadApp
      showStoreButtons={true}
      buttonText={"Start my free trial"}
      addedClass={"containerCenter"}
      mainContainerStyle={{
        flexDirection: "column",
        display: "flex",
        justifyContent: "center",
      }}
    />
  );

  return (
    <div
      className="full_bloc"
      style={{
        backgroundColor: ColorGuide.mainOrange,
        overflow: "hidden",
        ...style,
      }}
    >
      <img
        style={{
          zIndex: -1,
          position: "absolute",
          top: -90,
          right: 100,
          width: 300,
        }}
        src="/art/doubleCircleThin.png"
        alt="stock market forecast"
      />

      <div className="container containerRow">
        <div className="halfBloc">
          <h4>GET STARTED</h4>

          <h2
            style={{
              fontSize: responsiveWidth < 650 && 23,
              width: "100%",
              color: ColorGuide.mainBlack,
              marginBottom: 30,
            }}
          >
            Free trial
          </h2>

          {children}

          <div
            className="containerRowMobile"
            style={{ zIndex: 1500, marginBottom: responsiveWidth < 650 && 50 }}
          >
            <img
              className="shadowOrange"
              style={{
                width: responsiveWidth < 650 ? 50 : 70,
                height: responsiveWidth < 650 ? 50 : 70,
                marginRight: 15,
                borderRadius: 15,
              }}
              src="/appIcon.png"
            />

            <p
              style={{
                color: "black",
                lineHeight: 1.2,
                maxWidth: 400,
              }}
            >
              {tagLine}
            </p>
          </div>
        </div>

        <div className="halfBloc" style={{ justifyContent: "center" }}>
          <img
            style={mockupStyle}
            id={styles.mockupWebMobile}
            src={mockupSource}
          />

          {!isMobileLayout &&
            renderDownloadLinkList({
              addedClass: "containerMobileHidden",
            })}
        </div>

        {isMobileLayout &&
          renderDownloadLinkList({
            addedClass: "containerOnlyMobileFlex",
          })}
      </div>
    </div>
  );
}
