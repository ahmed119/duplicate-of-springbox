import React, { useState } from "react";

import Head from "next/head";
import Link from "next/link";
import { toast } from "react-hot-toast";

import axios from "axios";

import styles from "../styles/Story.module.css";
import ColorGuide from "../styles/colorGuide";

import Layout from "../components/layout";
import ArticlePreview from "../components/articlePreview";
import FollowNewsletter from "../components/followNewsletter";
import DownloadApp from "../components/downloadApp";

import ModalFreeEbook from "../modals/modalFreeEbook";

import useWindowSize from "../hooks/useWindowSize";

import dataFAQ from "../data/dataFAQ";
import articlesList from "../data/articlesList";

import firebase from "../config/firebase";

const addToNewsletterAPI =
  "https://us-central1-springbox-67517.cloudfunctions.net/marketingAutomation-subscribeToNewsletter";

export default function InputMailDownload({
  isSignup = false,
  afterMailSent = null,
}) {
  const { isMobileLayout } = useWindowSize();

  const [userMail, setUserMail] = useState(null);

  const checkIfEmailIsValid = (mailToTest) => {
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(mailToTest);
  };

  const addUserToNewsletter = async () => {
    try {
      if (!userMail || !checkIfEmailIsValid(userMail.trim())) {
        throw new Error("Please enter a valid email address.");
      }

      await firebase
        .database()
        .ref(`newsletterSubscribers`)
        .push({ userMail, subcribed: true });

      if (afterMailSent) {
        afterMailSent();
      }

      toast.success("Subscribed to newsletter!");
    } catch (e) {
      console.log(e);
      toast.error(e.message);
    }
  };

    const handleClick = (event) => {
       if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ event:'subscribe' });
    }
  };

  return (
    <div className="containerRow">
      <input
        name="mail"
        type="text"
        placeholder="Email"
        style={
          isMobileLayout
            ? { width: "80%", marginBottom: 20 }
            : { width: "80%", marginRight: 20 }
        }
        value={userMail}
        onChange={(event) => setUserMail(event.target.value)}
      />

      <button
        onClick={() => addUserToNewsletter(),handleClick}
        className="shadowBlack"
        style={{
          cursor: "pointer",
          backgroundColor: isSignup
            ? ColorGuide.mainBlue
            : ColorGuide.mainWhite,
          paddingRight: 30,
          paddingLeft: 30,
          borderWidth: 0,
        }}
      >
        <img
          src={isSignup ? "/UI/chevronWhite.png" : "/UI/chevronBlue.png"}
          style={{ width: 15, marginRight: 10 }}
          alt="Springbox AI Whitepaper"
        />

        <p
          style={{
            color: isSignup ? ColorGuide.mainWhite : ColorGuide.mainBlue,
            fontWeight: "600",
            marginRight: 5,
          }}
        >
          {isSignup ? "Signup" : "Download"}
        </p>
      </button>
    </div>
  );
}
