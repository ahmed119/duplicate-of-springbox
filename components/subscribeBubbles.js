import React, { useState } from "react";

import Link from "next/link";

import ColorGuide from "../styles/colorGuide";

import useWindowSize from "../hooks/useWindowSize";

import styles from "./footer.module.css";

export default function SubscribeBubbles() {
  const [currentSubscriptionPack, setCurrentSubscriptionPack] = useState(0);

  const { width, height } = useWindowSize();

  const defaultBubbleSize =
    150 *
    (width < 350
      ? 0.7
      : width < 400
      ? 0.85
      : width < 650
      ? 0.87
      : width < 1000
      ? 0.85
      : 1);

  const subscriptionPacks = [
    {
      title: "6 Months Access",
      pricePerDay: "1.95**",
    },
    {
      title: "Monthly Access",
      pricePerDay: "2.45*",
    },
  ];

  return (
    <div
      className="containerSpaceBetween"
      style={{
        width:
          width < 350
            ? 270
            : width < 450
            ? 330
            : width < 650
            ? 340
            : width < 1000
            ? 330
            : 380,
        margin: width < 650 ? "0px auto" : "initial",
      }}
    >
      {subscriptionPacks.map(({ title, pricePerDay, subText }, index) => (
        <div
          onClick={() => setCurrentSubscriptionPack(index)}
          className="containerCenter"
          style={{
            cursor: "pointer",
            position: "relative",
            transition: "1s",
            flexDirection: "column",
            width:
              currentSubscriptionPack === index
                ? defaultBubbleSize * 1.5
                : defaultBubbleSize,
            height:
              currentSubscriptionPack === index
                ? defaultBubbleSize * 1.5
                : defaultBubbleSize,
            marginBottom: 20,
            borderRadius: "50%",
            backgroundColor:
              currentSubscriptionPack === index ? ColorGuide.mainBlue : "white",
          }}
        >
          <p
            className="small"
            style={{
              fontSize:
                width < 350 ? 10 : width < 650 ? 13 : width < 1000 ? 12 : 14,
              fontWeight: "600",
              textDecoration: "underline",
              marginBottom: 10,
              color:
                currentSubscriptionPack === index
                  ? ColorGuide.mainWhite
                  : ColorGuide.mainBlue,
            }}
          >
            {title}
          </p>

          <h2
            style={{
              fontFamily: "WorkSans Bold",
              fontSize:
                currentSubscriptionPack === index
                  ? width < 350
                    ? 30
                    : width < 650
                    ? 35
                    : 40
                  : width < 350
                  ? 20
                  : 28,
              marginBottom: 0,
              color:
                currentSubscriptionPack === index
                  ? ColorGuide.mainWhite
                  : ColorGuide.mainOrange,
            }}
          >
            ${pricePerDay}
          </h2>

          <p
            className="small"
            style={{
              fontFamily: "WorkSans Bold",
              color:
                currentSubscriptionPack === index
                  ? ColorGuide.mainWhite
                  : ColorGuide.mainOrange,
            }}
          >
            / Trading Day
          </p>

          {index === 0 && (
            <div
              className="containerCenter"
              style={{
                transition: "1.5s",
                position: "absolute",
                top: currentSubscriptionPack === index ? 0 : -30,
                left: currentSubscriptionPack === index ? 0 : -10,
                height: defaultBubbleSize * 0.4,
                width: defaultBubbleSize * 0.4,
                borderRadius: 150,
                backgroundColor: ColorGuide.mainYellow,
              }}
            >
              <p
                style={{
                  fontSize: 13,
                  fontWeight: "600",
                  color: ColorGuide.mainBlue,
                  textAlign: "center",
                  lineHeight: 1,
                }}
              >
                SAVE
                <br />
                20%
              </p>
            </div>
          )}
        </div>
      ))}
    </div>
  );
}
