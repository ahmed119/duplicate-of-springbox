import React from "react";
import ColorGuide from "../styles/colorGuide";

const RenderText = ({ data }) => {
  if (data) {
    return data.map(({ type, text, spans }, index) => {
      if (type === "paragraph") {
        return (
          <p
            className={`${index === 0 && "drop-cap"}`}
            style={{
              marginBottom: 40,
              fontWeight: spans[0]?.type === "strong" && "600",
              color: spans[0]?.type === "strong" && ColorGuide.mainBlue,
            }}
          >
            {text}
          </p>
        );
      } else if (type === "heading2") {
        return (
          <h3
            style={{
              color: ColorGuide.mainOrange,
              marginBottom: 10,
            }}
          >
            {text}
          </h3>
        );
      }
    });
  }

  return null;
};

export default RenderText;
