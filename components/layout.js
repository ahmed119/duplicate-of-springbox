import Head from "next/head";
import { Toaster } from "react-hot-toast";

import Menu from "./menu";
import Footer from "./footer";

import useWindowSize from "../hooks/useWindowSize";

export default function Layout({ children, pageTitle }) {
  const { width: responsiveWidth, height: responsiveHeight } = useWindowSize();

  return (
    <div className="wrapper">
      <Head>
        <title>{pageTitle} | Springbox AI</title>
        <meta
          name="viewport"
          content={`initial-scale=1.0, width=${
            responsiveWidth > 750
              ? "width=1100"
              : "width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no"
          }`}
        />


        <meta name="apple-itunes-app" content="app-id=1533228624" />

        <meta itemprop="url" content="https://springbox.ai" />

        <meta
          name="description"
          content="Stay ahead of the game with the Advanced Stock Market Forecasts you've been waiting for: powered by AI, driven by accuracy: free trial, Get The app!"
        />

        <meta
          name="keywords"
          content="trading, profit, stock, etfs, forex, indices, commodities"
        />

        <meta name="author" content="Springbox AI" />

        <meta
          itemprop="name"
          content="Springbox - Advanced Trading Forecast."
        />
        <meta
          itemprop="description"
          content="Stay ahead of the game with the Advanced Stock Market Forecasts you've been waiting for: powered by AI, driven by accuracy: free trial, Get The app!"
        />

        <meta
          name="facebook-domain-verification"
          content="ub8vq5vsfu343n4o1h2j7ojie4n9fx"
        />

        <meta
          property="og:title"
          content="Springbox - Advanced Trading Forecast."
        />
        <meta
          property="og:description"
          content="Stay ahead of the game with the Advanced Stock Market Forecasts you've been waiting for: powered by AI, driven by accuracy: free trial, Get The app!"
        />

        <meta
          name="twitter:title"
          content="Springbox - Advanced Trading Forecast."
        />
        <meta
          name="twitter:description"
          content="Stay ahead of the game with the Advanced Stock Market Forecasts you've been waiting for: powered by AI, driven by accuracy: free trial, Get The app!"
        />

        <link
          href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;500;600;700;800;900&display=swap"
          rel="stylesheet"
        />
      </Head>

      <Menu />

      {children}

      <Footer />

      <Toaster
        toastOptions={{
          style: {
            fontFamily: "Work Sans",
          },
        }}
      />
    </div>
  );
}
