import React, { useEffect } from "react";

import ColorGuide from "../styles/colorGuide";

export default function ModalContainer({
  children,

  isVisible,
  setIsVisible = null,
  containerStyle,
}) {
  useEffect(() => {
    if (isVisible) {
      document.body.style.overflow = "hidden";
    }
    return () => (document.body.style.overflow = "unset");
  }, [isVisible]);

  if (!isVisible) {
    return null;
  }

  return (
    <div
      className="containerCenter"
      style={{
        flex: 1,
        flexDirection: "column",
        position: "fixed",
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        overflowY: "hidden",
      }}
    >
      <div
        onClick={setIsVisible ? () => setIsVisible(false) : null}
        style={{
          zIndex: "-1",
          position: "absolute",
          top: 0,
          right: 0,
          left: 0,
          bottom: 0,
          backgroundColor: "rgba(0,0,0,0.3)",
        }}
      />

      <div className="containerItem" style={{ zIndex: 1, ...containerStyle }}>
        {children}
      </div>
    </div>
  );
}
