import Link from "next/link";
import { useState } from "react";
import Slider from "react-slick";

import DownloadApp from "../components/downloadApp";

import useWindowSize from "../hooks/useWindowSize";

import ColorGuide from "../styles/colorGuide";

import styles from "./appFooter.module.css";

import { downloadLinkList } from "../data/mainConfig";

export default function SliderContainer({
  children,
  data = [],
  desktopClass = null,
  renderInitial,
  sliderContainerStyle = {},
  forceSlider = false,
}) {
  const { isMobileLayout } = useWindowSize();

  const sliderBreakpoint = forceSlider || isMobileLayout;

  if (!sliderBreakpoint) {
    return (
      <div className={`${desktopClass}`}>
        {data.map((item, index) => renderInitial(item, index))}
      </div>
    );
  }

  return (
    <Slider {...defaultSliderSettings}>
      {data.map((item, index) => (
        <div style={sliderContainerStyle}>{renderInitial(item, index)}</div>
      ))}
    </Slider>
  );
}

const defaultSliderSettings = {
  dots: true,
  infinite: true,
  speed: 500,
  variableWidth: true,
  arrows: false,

  centerPadding: "30px",

  slidesToShow: 3,
  slidesToScroll: 3,
  initialSlide: 0,

  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 650,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};
