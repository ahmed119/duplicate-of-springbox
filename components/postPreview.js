import React from "react";
import { RichText, Date } from "prismic-reactjs";
import { default as NextLink } from "next/link";

import useWindowSize from "../hooks/useWindowSize";

import FirstParagraph from "../components/firstParagraph";

import ColorGuide from "../styles/colorGuide";

import { hrefResolver, linkResolver } from "../prismic-configuration.js";

const dateFormat = {
  month: "long",
  day: "2-digit",
  year: "numeric",
};

const PostPreview = (params) => {
  const {
    post: {
      data: { postheroimage, posttitle, postauthor, postcreatedat, posttext },
    },
    isHero,
  } = params;

  const { width: responsiveWidth, height: responsiveHeight } = useWindowSize();

  if (params.post) {
    return (
      <div
        style={{
          width: responsiveWidth < 650 ? "100%" : isHero ? "48%" : "30%",
          marginBottom: responsiveWidth < 650 && 50,
        }}
      >
        <p
          style={{
            color: ColorGuide.mainOrange,
            textDecoration: "underline",
            marginBottom: 15,
          }}
        >
          {new Intl.DateTimeFormat("en-US", dateFormat).format(
            Date(postcreatedat)
          )}
        </p>

        <div
          style={{
            position: "relative",
            width: "100%",
            height: 250,
            borderRadius: 10,
            overflow: "hidden",
            marginBottom: 30,
          }}
        >
          <img className="imageFit" src={postheroimage.url} />

          {isHero && (
            <img
              style={{ width: 40, position: "absolute", right: 40 }}
              src="/UI/blogHero.png"
            />
          )}
        </div>

        <h3 style={{ color: ColorGuide.mainBlue, marginBottom: 20 }}>
          {RichText.asText(posttitle)}
        </h3>

        <FirstParagraph
          style={{ marginBottom: 35, width: "80%" }}
          sliceZone={posttext}
        />

        <NextLink
          as={linkResolver(params.post)}
          href={hrefResolver(params.post)}
        >
          <button
            className="containerCenter"
            style={{ borderColor: ColorGuide.mainBlue }}
          >
            <img className="buttonArrow" src="/UI/arrowBlue.png" />
            <p style={{ color: ColorGuide.mainBlue }}>Read More</p>
          </button>
        </NextLink>
      </div>
    );
  }

  return null;
};

export default PostPreview;
