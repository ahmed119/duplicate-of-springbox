import React, { useState } from "react";

import Link from "next/link";
import QRCode from "react-qr-code";

import ColorGuide from "../styles/colorGuide";

import useWindowSize from "../hooks/useWindowSize";

import styles from "./footer.module.css";

import { downloadLinkList } from "../data/mainConfig";

export default function SubscribeQR() {
  const [currentSubscriptionPack, setCurrentSubscriptionPack] = useState(0);

  const { width, height } = useWindowSize();

  const defaultPadding = width < 650 ? 30 : 50;

  const premiumBenefits = [
    "Advanced Markets Forecasting",
    "Smart trading signals with AI-insights",
    "Graph and Live Markets Data",
    "Springbox.ai Boost & more...",
  ];

  return (
    <div className="" style={{ display: "flex", justifyContent: "center" }}>
      {downloadLinkList.map(({ link, source }, index) => (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            marginRight: index === 0 && "10%",
          }}
        >
          {width > 650 && (
            <div style={{ marginBottom: 20 }}>
              <QRCode
                size={width < 850 ? 90 : width < 1024 ? 120 : 100}
                value={link}
              />
            </div>
          )}

          <Link
            style={{ display: "block", marginRight: 0, marginLeft: 0 }}
            href={link}
          >
            <img
              alt="stock trading app"
              key={index}
              style={{
                display: "flex",
                alignSelf: "flex-end",
                width:
                  width < 350
                    ? 110
                    : width < 650
                    ? 140
                    : width < 850
                    ? 120
                    : 100,
              }}
              src={source}
            />
          </Link>
        </div>
      ))}
    </div>
  );
}
