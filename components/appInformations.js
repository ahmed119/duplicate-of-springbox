import Link from "next/link";

import useWindowSize from "../hooks/useWindowSize";

import ColorGuide from "../styles/colorGuide";

import styles from "./appInformations.module.css";

import { downloadLinkList } from "../data/mainConfig";

export default function AppInformations({
  style,
  title,
  tagLine,
  children,
  buttonColor,
  mockupSource,
  mockupStyle,
}) {
  const { width: responsiveWidth, height: responsiveHeight } = useWindowSize();

  const renderDownloadLinkList = ({ addedClass }) => (
    <div
      className={`containerRowMobile ${
        responsiveWidth < 650 && "containerCenter"
      } ${addedClass}`}
      style={{
        width: responsiveWidth < 414 ? 400 : 500,
      }}
    >
      {downloadLinkList.map(({ source, link }, index) => (
        <Link href={link}>
          <img
            style={{ cursor: "pointer", marginRight: index === 0 && 15 }}
            key={index}
            className={styles.iconStoreDownload}
            src={source}
            alt="stock trading app"
          />
        </Link>
      ))}
    </div>
  );

  const renderGetTheApp = () => (
    <div
      className="containerRowMobile"
      style={{
        alignItems: "center",
        marginBottom: 15,
      }}
    >
      <img src="/UI/getStartedBlack.png" style={{ marginRight: 10 }} />

      <h3
        style={{
          color: ColorGuide.mainBlack,
        }}
      >
        Get the App
      </h3>
    </div>
  );

  const renderClassicButton = () => (
    <Link href={"/pricing"}>
      <button
        className="containerCenter"
        style={{ borderColor: ColorGuide.mainBlue, marginBottom: 30 }}
      >
        <img className="buttonArrow" src={"/UI/arrowBlue.png"} />
        <p style={{ color: ColorGuide.mainBlue }}>Download the App</p>
      </button>
    </Link>
  );

  return (
    <div
      className="full_bloc"
      style={{
        backgroundColor: ColorGuide.mainOrange,
        overflow: "hidden",
        ...style,
      }}
    >
      <div className="container containerRow">
        <div className="halfBloc">
          <h3
            style={{
              width: "100%",
              color: ColorGuide.mainBlue,
              marginBottom: 50,
            }}
          >
            {title}
          </h3>

          {children}

          <div
            className="containerRowMobile"
            style={{ zIndex: 1500, marginBottom: 50 }}
          >
            <img
              style={{
                width: responsiveWidth < 650 ? 50 : 70,
                height: responsiveWidth < 650 ? 50 : 70,
                marginRight: 15,
              }}
              src="/appIcon.png"
            />

            <p
              style={{
                color: "black",
                fontWeight: "600",
                lineHeight: 1.2,
                maxWidth: 400,
              }}
            >
              {tagLine}
            </p>
          </div>

          {responsiveWidth > 650 && renderClassicButton()}

          {renderDownloadLinkList({
            width: 500,
            addedClass: "containerMobileHidden",
          })}
        </div>

        <div
          style={{ position: "relative" }}
          className="halfBloc containerCenter"
        >
          <div
            className="containerCenter"
            style={{
              zIndex: 50,
              flexDirection: "column",
              position: "absolute",
              top: responsiveWidth < 650 ? 10 : -180,
              right:
                responsiveWidth < 650
                  ? "initial"
                  : responsiveWidth < 1200
                  ? "60%"
                  : "50%",
              left: responsiveWidth < 650 ? "5vw" : "initial",
              height: 85,
              width: 85,
              borderRadius: "50%",
              backgroundColor: ColorGuide.mainBlue,
            }}
          >
            <p
              className="small"
              style={{
                color: ColorGuide.mainWhite,
              }}
            >
              From
            </p>

            <h3
              style={{
                fontSize: 20,
                color: ColorGuide.mainOrange,
                marginBottom: 0,
              }}
            >
              $1.95
            </h3>
            <p
              className="small"
              style={{ fontSize: 8, fontWeight: "600", color: "white" }}
            >
              / trading day
            </p>
          </div>

          <img
            style={mockupStyle}
            id={styles.mockupWebMobile}
            src={mockupSource}
          />
        </div>

        {responsiveWidth < 650 && renderGetTheApp()}

        {renderDownloadLinkList({
          addedClass: "containerOnlyMobileFlex",
        })}
      </div>
    </div>
  );
}
