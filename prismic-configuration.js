import Prismic from "prismic-javascript";

export const apiEndpoint = "https://wwwspringboxai.cdn.prismic.io/api/v2";
export const accessToken =
  "MC5YMFVpd1JBQUFDSUFGWUZj.77-9KAnvv70o77-977-977-977-9Ee-_ve-_vQgZN2ZP77-977-977-977-9bu-_ve-_ve-_ve-_ve-_vRrvv70n77-977-9";

export const Client = (req = null) =>
  Prismic.client(apiEndpoint, createClientOptions(req, accessToken));

const createClientOptions = (req = null, prismicAccessToken = null) => {
  const reqOption = req ? { req } : {};
  const accessTokenOption = prismicAccessToken
    ? { accessToken: prismicAccessToken }
    : {};
  return {
    ...reqOption,
    ...accessTokenOption,
  };
};

export const linkResolver = (doc) => {
  if (doc.type === "blog_post") {
    return `/${doc.uid}`;
  }
  return "/";
};

// Additional helper function for Next/Link components
export const hrefResolver = (doc) => {
  if (doc.type === "blog_post") {
    return `/post?uid=${doc.uid}`;
  }
  return "/";
};
