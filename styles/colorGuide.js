export default {
  darkBlue: "#0024BD",
  mainBlue: "#0023D2",
  lightBlue: "#E0E5F2",
  mediumBlue: "#ccd3f6",
  superLightBlue: "rgba(0,36,189,0.05)",

  mainOrange: "#FF9271",
  mediumOrange: "#ffe9e2",
  lightOrange: "rgba(255,145,110,0.1)",

  mainYellow: "#E6FF02",
  mainGreen: "#2E5117",

  mainWhite: "#fff",
  mediumWhite: "#ffffff80",
  mainBlack: "#000",

  lightGrey: "#f9f9fb",
  mediumGrey: "#979797",
  mainGrey: "#686868",
};
