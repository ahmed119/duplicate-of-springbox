import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import styles from "../styles/WhitePaper.module.css";
import ColorGuide from "../styles/colorGuide";

import Layout from "../components/layout";


export default function WhitePaper() {
  useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);
  
  return (
    <Layout pageTitle="White Paper">
      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.lightBlue }}
      >
        <div className="container" id={styles.containerIntro}>
          <div className="halfBloc">
            <h1 style={{ color: ColorGuide.mainBlue, width: "80%" }}>
              White Paper
            </h1>

            <p style={{ color: ColorGuide.mainBlue }}>
              <span style={{ fontWeight: "600" }}>
                Springbox AI may be powered by advanced algorithms, but it’s
                made by real people, sharing the same ideals.
              </span>
              <br />
              <br />
              And we’re on a mission to reinvent asset management by using
              advanced AI trading algorithms in a democratic way.
            </p>
          </div>

          <img
            style={{
              position: "absolute",
              right: 0,
              top: "21%",
              height: "60%",
            }}
            src="/art/whitePaper.png"
          />
        </div>
      </div>

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainWhite }}
      >
        <div className="container">
          <h2
            style={{
              color: ColorGuide.mainOrange,
              textDecoration: "underline",
              marginBottom: 40,
            }}
          >
            What is Springbox AI about?
          </h2>

          <p style={{ color: ColorGuide.mainBlue }}>
            Springbox AI is an ultimate platform offering live trading forecast
            services. Whether you are a trading expert, or just starting your
            trading journey, Springbox AI will become your number-one trading
            assistant. Accurate, detailed and up-to-date, our trading signals
            will definitely facilitate your trading routine and help you boost
            your trading profits. Springbox AI is an ultimate platform offering
            live trading forecast services. Whether you are a trading expert, or
            just starting your trading journey, Springbox AI will become your
            number-one trading assistant. Accurate, detailed and up-to-date, our
            trading signals will definitely facilitate your trading routine and
            help you boost your trading profits.Springbox AI is an ultimate
            platform offering live trading forecast services. Whether you are a
            trading expert, or just starting your trading journey, Springbox AI
            will become your number-one trading assistant. Accurate, detailed
            and up-to-date, our trading signals will definitely facilitate your
            trading routine and help you boost your trading profits.
            <br />
            <br />
            Springbox AI is an ultimate platform offering live trading forecast
            services. Whether you are a trading expert, or just starting your
            trading journey, Springbox AI will become your number-one trading
            assistant. Accurate, detailed and up-to-date, our trading signals
            will definitely facilitate your trading routine and help you boost
            your trading profits. Springbox AI is an ultimate platform offering
            live trading forecast services. Whether you are a trading expert, or
            just starting your trading journey, Springbox AI will become your
            number-one trading assistant. Accurate, detailed and up-to-date, our
            trading signals will definitely facilitate your trading routine and
            help you boost your trading profits.Springbox AI is an ultimate
            platform offering live trading forecast services. Whether you are a
            trading expert, or just starting your trading journey, Springbox AI
            will become your number-one trading assistant. Accurate, detailed
            and up-to-date, our trading signals will definitely facilitate your
            trading routine and help you boost your trading profits.
            <br />
            <br />
            Springbox AI is an ultimate platform offering live trading forecast
            services. Whether you are a trading expert, or just starting your
            trading journey, Springbox AI will become your number-one trading
            assistant. Accurate, detailed and up-to-date, our trading signals
            will definitely facilitate your trading routine and help you boost
            your trading profits. Springbox AI is an ultimate platform offering
            live trading forecast services. Whether you are a trading expert, or
            just starting your trading journey, Springbox AI will become your
            number-one trading assistant. Accurate, detailed and up-to-date, our
            trading signals will definitely facilitate your trading routine and
            help you boost your trading profits.Springbox AI is an ultimate
            platform offering live trading forecast services. Whether you are a
            trading expert, or just starting your trading journey, Springbox AI
            will become your number-one trading assistant. Accurate, detailed
            and up-to-date, our trading signals will definitely facilitate your
            trading routine and help you boost your trading profits.
            <br />
            <br />
            Springbox AI is an ultimate platform offering live trading forecast
            services. Whether you are a trading expert, or just starting your
            trading journey, Springbox AI will become your number-one trading
            assistant. Accurate, detailed and up-to-date, our trading signals
            will definitely facilitate your trading routine and help you boost
            your trading profits. Springbox AI is an ultimate platform offering
            live trading forecast services. Whether you are a trading expert, or
            just starting your trading journey, Springbox AI will become your
            number-one trading assistant. Accurate, detailed and up-to-date, our
            trading signals will definitely facilitate your trading routine and
            help you boost your trading profits.Springbox AI is an ultimate
            platform offering live trading forecast services. Whether you are a
            trading expert, or just starting your trading journey, Springbox AI
            will become your number-one trading assistant. Accurate, detailed
            and up-to-date, our trading signals will definitely facilitate your
            trading routine and help you boost your trading profits.
          </p>
        </div>
      </div>
    </Layout>
  );
}
