import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import Link from "next/link";

import styles from "../styles/Story.module.css";
import ColorGuide from "../styles/colorGuide";

import Layout from "../components/layout";
import FollowNewsletter from "../components/followNewsletter";
import RenderFAQ from "../components/renderFAQ";
import useWindowSize from "../hooks/useWindowSize";

import dataFAQ from "../data/dataFAQ";


export default function Story() {

  useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);
  const { isMobileLayout, isTabletPortraitLayout } = useWindowSize();

  const [currentFAQIndex, setCurrentFAQIndex] = useState(0);

  const teamData = [
    {
      name: "Fayze Bouaouid",
      role: "CEO",
      picture: "/team/fayze.png",
      linkedin: "https://www.linkedin.com/in/fayzebouaouid/",
      description:
        "With nearly two decades in the banking and asset management industries, Fayze has spent his career learning every detail of financial markets, banking and investing. He brings his wealth of asset management knowledge to Springbox AI, in the service of assisting young investors find success. Fayze is working toward a democratized future of investing for everyone.",
    },
    {
      name: "Kassem Lahham",
      role: "Chief Strategic Officer",
      picture: "/team/kassem.png",
      linkedin: "https://www.linkedin.com/in/kassemlahham/",
      description:
        "Kassem’s career path spans over more than three decades in the financial industry. A financial architect with in-depth knowledge and experience, Kassem takes a very goal-oriented approach to passing on advice and assistance. Kassem is a successful FinTech entrepreneur and has forged a path that allows him the freedom to pass on his banking and finance knowledge to a new generation of investors.",
    },
    {
      name: "Mostafa Belkhayate",
      role: "Chief AI Strategist",
      picture: "/team/mostafa.png",
      linkedin: "https://www.linkedin.com/in/belkhayate/",
      description:
        "Fund Manager, Mostafa brings his experience in trading and his knowledge of AI systems as applied to financial investing to Springbox AI, which utilizes machine learning to help users make smart and fast financial decisions. One of Mostafa’s funds was ranked in 2006-2007 by Bloomberg as the best Fund in its category, earning him the best Gold Manager of the year.",
    },
  ];

  const decorationItemSize = isMobileLayout ? 60 : 140;

  return (
    <Layout pageTitle="Our Story">
      <div className="full_bloc">
        <div className="container" style={{ position: "relative" }}>
          <img
            style={{
              position: "absolute",
              top: isMobileLayout ? 20 : 40,
              right: "35%",
              width: 60,
            }}
            src="/art/accordeon.png"
            alt="stock market forecast"
          />

          <img
            style={{
              position: "absolute",
              top: isMobileLayout ? 20 : 80,
              left: isMobileLayout ? "8%" : "32%",
              width: 60,
              transform: "rotate(90deg)",
            }}
            src="/art/accordeon.png"
            alt="stock market forecast"
          />

          <div
            className="containerSpaceBetween"
            style={{
              width: isMobileLayout
                ? "100%"
                : isTabletPortraitLayout
                ? "80%"
                : "60%",
              alignSelf: "center",
              margin: "0 auto",
              marginBottom: 50,
            }}
          >
            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/theTeamLeft.png"
              alt="stock market forecast"
            />

            <h1
              style={{
                textAlign: "center",
              }}
            >
              The Team
            </h1>

            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/theTeamRight.png"
              alt="stock market forecast"
            />
          </div>

          <div
            className="containerSpaceBetween"
            id={styles.containerTeam}
            style={{ alignItems: "flex-start" }}
          >
            {teamData.map(
              ({ name, role, linkedin, picture, description }, index) => (
                <div
                  key={index}
                  className={"containerCenter"}
                  style={{
                    flex: 1,
                    flexDirection: "column",
                  }}
                >
                  <div
                    style={{
                      position: "relative",
                      width: 140,
                      height: 140,
                      marginBottom: 30,
                      borderRadius: "50%",
                      borderWidth: 3,
                      borderColor: ColorGuide.mainOrange,
                      borderStyle: "solid",
                      overflow: "hidden",
                    }}
                  >
                    <img
                      style={{
                        filter: "grayscale(100%)",
                        width: 140,
                        height: 140,
                      }}
                      src={picture}
                    />
                  </div>

                  <h3>{name}</h3>
                  <h3
                    style={{
                      marginBottom: 30,
                      fontWeight: "300",
                    }}
                  >
                    {role}
                  </h3>

                  <p
                    style={{
                      textAlign: "center",
                      width: "90%",
                      marginBottom: 30,
                    }}
                  >
                    {description}
                  </p>

                  <Link href={linkedin}>
                    <div
                      className="containerCenter"
                      style={{
                        cursor: "pointer",
                        height: 30,
                        width: 30,
                        backgroundColor: ColorGuide.lightOrange,
                        borderRadius: 5,
                      }}
                    >
                      <img
                        style={{
                          width: "40%",
                          height: "40%",
                        }}
                        src="/social/linkedinBlue.png"
                      />
                    </div>
                  </Link>
                </div>
              )
            )}
          </div>
        </div>
      </div>

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainGreen }}
      >
        <div
          className="container containerCenter"
          style={{ flexDirection: "column" }}
        >
          <h2
            style={{
              width: isMobileLayout ? "100%" : "80%",
              textAlign: "center",
              color: "white",
              marginBottom: 0,
            }}
          >
            Springbox AI may be powered by advanced algorithms, but it’s{" "}
            <span style={{ color: ColorGuide.mainOrange }}>
              made by real people
            </span>
            , sharing the same ideals.
          </h2>
        </div>
      </div>

      <div className="full_bloc">
        <div
          className={`container containerRow fullWidthMobile`}
          style={{ padding: 0 }}
        >
          <div className="halfBloc containerIllustration containerCenter">
            <img
              style={{ width: isMobileLayout ? "100%" : "75%" }}
              src="/art/revolutionaryApproach.png"
            />
          </div>

          <div className={`halfBloc alternateBloc containerMobile`}>
            <h4 style={{ marginBottom: 10 }}>THE TEAM</h4>

            <h2 style={{ marginBottom: 30 }}>A revolutionary approach</h2>

            <p style={{ marginBottom: 20 }}>
              We worked together with a highly-specialised group of scientists
              and researchers from the fields of artificial intelligence,
              mathematics, quantitative trading, and computer science to come up
              with a revolutionary approach, breaking with the past and aiming
              for the future.
            </p>

            <p style={{ marginBottom: 30 }}>
              Because we believe that the future of trading is here, it’s
              data-driven and more inclusive than ever before.
            </p>

            <Link href={"/pricing"}>
              <button
                className="shadowBlack"
                style={{
                  cursor: "pointer",
                  backgroundColor: ColorGuide.mainBlue,
                  padding: 15,
                  paddingRight: 30,
                  paddingLeft: 30,
                  borderWidth: 0,
                }}
              >
                <p
                  style={{
                    color: ColorGuide.mainWhite,
                    fontWeight: "600",
                    marginRight: 10,
                  }}
                >
                  Get started
                </p>

                <img
                  style={{ width: 12 }}
                  src="/UI/chevronWhite.png"
                  alt="Springbox AI Newsletter"
                />
              </button>
            </Link>
          </div>
        </div>
      </div>

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.lightBlue }}
      >
        <div className="container">
          <div
            className="containerSpaceBetween"
            style={{
              width: isMobileLayout
                ? "100%"
                : isTabletPortraitLayout
                ? "80%"
                : "60%",
              alignSelf: "center",
              margin: "0 auto",
              marginBottom: 50,
            }}
          >
            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/faqLeft.png"
              alt="stock market forecast"
            />

            <h1
              style={{
                textAlign: "center",
              }}
            >
              FAQ
            </h1>

            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/faqRight.png"
              alt="stock market forecast"
            />
          </div>

          <RenderFAQ dataFAQ={dataFAQ} />
        </div>
      </div>

      <FollowNewsletter />
    </Layout>
  );
}
