import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";

import styles from "../styles/Story.module.css";
import ColorGuide from "../styles/colorGuide";

import Layout from "../components/layout";
import ArticlePreview from "../components/articlePreview";
import FollowNewsletter from "../components/followNewsletter";
import DownloadApp from "../components/downloadApp";
import InputMailDownload from "../components/inputMailDownload";

import ModalFreeEbook from "../modals/modalFreeEbook";

import useWindowSize from "../hooks/useWindowSize";

import dataFAQ from "../data/dataFAQ";
import articlesList from "../data/articlesList";

import { downloadFile } from "../helpers/helpers";



export default function ThoughtLeadership() {

  useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);

  const {
    width: responsiveWidth,
    height: responsiveHeight,
    isMobileLayout,
    isTabletLandscapeLayout,
    isTabletPortraitLayout,
  } = useWindowSize();

  const router = useRouter();

  const [showModalFreeBook, setShowModalFreeBook] = useState(false);

  const ebookList = [
    {
      title: "Find out how to leverage AI for a profitable side hustle...",
      image: "/books/profitableSideHustle.png",
      downloadLink: "/books/profitableSideHustle.pdf",
      fileName: "profitableSideHustle",
    },
    {
      title:
        "Find out why you should consider adding artificial intelligence to your investment toolkit.",
      image: "/books/addingArtificialIntelligence.png",
      downloadLink: "/books/addingArtificialIntelligence.pdf",
      fileName: "addingArtificialIntelligence",
    },
    { image: "/books/comingSoon.png", downloadLink: "#" },
  ];

  const decorationItemSize = isMobileLayout ? 60 : 140;
  const showColumnLayout = isMobileLayout || isTabletPortraitLayout;

  return (
    <Layout pageTitle="Thought Leadership">
      <div className="full_bloc">
        <div className="container" style={{ position: "relative" }}>
          <img
            style={{
              position: "absolute",
              top: isMobileLayout ? 20 : 40,
              right: isMobileLayout ? "10%" : "35%",
              width: 60,
            }}
            src="/art/accordeon.png"
            alt="stock market forecast"
          />

          <img
            style={{
              position: "absolute",
              top: isMobileLayout ? 20 : 80,
              left: isMobileLayout ? "10%" : "32%",
              width: 60,
              transform: "rotate(0deg)",
            }}
            src="/art/accordeonBlue.png"
            alt="stock market forecast"
          />

          <div
            className="containerSpaceBetween"
            style={{
              width: isMobileLayout
                ? "100%"
                : isTabletPortraitLayout
                ? "80%"
                : "60%",
              alignSelf: "center",
              margin: "0 auto",
            }}
          >
            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/theTeamLeft.png"
              alt="stock market forecast"
            />

            <h1
              style={{
                textAlign: "center",
              }}
            >
              Thought
              <br />
              Leadership
            </h1>

            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/theTeamRight.png"
              alt="stock market forecast"
            />
          </div>
        </div>
      </div>

      <div className="full_bloc">
        <div className="container" style={{ paddingTop: 0 }}>
          <div
            className={`${!showColumnLayout && "containerSpaceBetween"}`}
            style={{ alignItems: "flex-start", paddingBottom: 0 }}
          >
            <div
              className="halfBloc"
              style={{
                width: showColumnLayout ? "100%" : "48%",
                marginBottom: showColumnLayout && 50,
              }}
            >
              <div
                style={{ marginBottom: 25 }}
                className="containerSpaceBetween"
              >
                <h2 style={{ margin: 0 }}>Featured article</h2>

                <Link href={"/articles"}>
                  <p
                    style={{
                      cursor: "pointer",
                      color: ColorGuide.mainOrange,
                      fontWeight: "600",
                      textDecoration: "underline",
                      marginRight: 20,
                    }}
                  >
                    Browse all
                  </p>
                </Link>
              </div>

              <ArticlePreview {...articlesList[0]} articleID={0} isPreview />
            </div>

            <div
              className="halfBloc"
              style={{ width: showColumnLayout ? "100%" : "48%" }}
            >
              <div className="containerRowMobile" style={{ marginBottom: 25 }}>
                <h2 style={{ margin: 0, marginRight: 10 }}>Discord chats</h2>
                <img
                  style={{
                    width: 40,
                  }}
                  src="/social/discord.png"
                  alt="Discord"
                />
              </div>

              <p style={{ marginBottom: 20 }}>
                Some of the best investment and Springbox insights come from our{" "}
                <span
                  style={{
                    textDecoration: "underline",
                    fontWeight: "bold",
                    color: ColorGuide.mainOrange,
                  }}
                >
                  Discord community
                </span>
                , so we thought we’d share...
              </p>

              <p style={{ fontWeight: "bold", marginBottom: 20 }}>
                #📡 - podcasts!
              </p>

              <img
                style={{
                  width: "50%",
                  borderRadius: 14,
                  marginBottom: 20,
                }}
                src="/placeholders/discordWomen.png"
                alt="Springbox AI Discord"
              />

              <Link href={"/articles"}>
                <button
                  className="containerCenter"
                  style={{ borderColor: ColorGuide.mainBlue }}
                >
                  <p style={{ color: ColorGuide.mainBlue }}>Launching Soon</p>
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.lightBlue }}
      >
        <img
          style={{
            position: "absolute",
            top: 0,
            right: isTabletLandscapeLayout ? -150 : 0,
            ...(isMobileLayout
              ? { width: 150, top: 250, right: 0 }
              : { height: "100%", zIndex: -1 }),
          }}
          src={
            isMobileLayout
              ? "/pictures/whitepaperManMobile.png"
              : "/pictures/whitepaperMan.png"
          }
          alt="Springbox AI Discord"
        />

        <img
          style={{
            zIndex: -1,
            position: "absolute",
            top: 150,
            left: 0,
            width: 120,
          }}
          src="/art/doubleCircleThinBlueLightMerged.png"
          alt="stock market forecast"
        />

        <div className="container containerRow">
          <img
            style={{
              width: 350,
              borderRadius: 14,
              marginBottom: 20,
            }}
            src="/art/whitePaper.png"
            alt="Springbox AI Whitepaper"
          />

          <div style={{ width: isMobileLayout ? "100%" : "40%" }}>
            <h4>WHITEPAPER</h4>

            <h2 style={{ marginBottom: 30 }}>
              Find out more about using AI for an extra income.
            </h2>

            <p style={{ marginBottom: 30 }}>
              Download our whitepaper with 5 tips to how to leverage AI to
              creating a side hustle.
            </p>

            <InputMailDownload
              afterMailSent={() =>
                downloadFile({
                  fileLink: "/books/whitePaper.pdf",
                  fileName: "whitePaper",
                })
              }
            />
          </div>
        </div>
      </div>

      <div className="full_bloc">
        <img
          style={{
            zIndex: -1,
            position: "absolute",
            top: 150,
            right: 0,
            width: 120,
            transform: "rotate(180deg)",
          }}
          src="/art/doubleCircleThinBlueLightMerged.png"
          alt="stock market forecast"
        />

        <div className="container" style={{ paddingBottom: 0 }}>
          <h4 style={{ fontWeight: "600" }}>FREE EBOOKS</h4>

          <div
            className={`${!isMobileLayout && "containerSpaceBetween"}`}
            style={{ marginRight: -65, marginLeft: -65 }}
          >
            {ebookList.map((currentBook, index) => {
              const { image, downloadLink } = currentBook;

              const buttonColor =
                index === 2 ? ColorGuide.mainGrey : ColorGuide.mainBlue;

              return (
                <div
                  className="containerCenter"
                  style={{
                    flexDirection: "column",
                    width: isMobileLayout ? "100%" : "32%",
                  }}
                >
                  <img
                    style={{
                      opacity: index === 2 ? 0.1 : 1,
                      width: "100%",
                      borderRadius: 14,
                      marginBottom: 0,
                    }}
                    src={image}
                    alt="Download Ebook"
                  />

                  <button
                    onClick={() =>
                      index !== 2 && setShowModalFreeBook(currentBook)
                    }
                    className="containerCenter"
                    style={{
                      borderColor: buttonColor,
                      marginTop:
                        isTabletPortraitLayout && !isMobileLayout ? -30 : -50,
                    }}
                  >
                    <p style={{ color: buttonColor }}>
                      {index === 2 ? "Coming Soon" : "Download"}
                    </p>
                  </button>
                </div>
              );
            })}
          </div>
        </div>
      </div>

      <FollowNewsletter />

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainBlue }}
      >
        <div
          className="container containerCenter"
          style={{ flexDirection: "column" }}
        >
          <h2
            style={{
              textAlign: "center",
              color: "white",
              width: showColumnLayout ? "90%" : "70%",
              margin: "0 auto",
              marginBottom: 30,
            }}
          >
            Get unlimited access and start making profits at a democratic price.{" "}
            <span style={{ color: ColorGuide.mainOrange }}>
              Cancel anytime.
            </span>
          </h2>

          <DownloadApp showStoreButtons={false} />
        </div>
      </div>

      <ModalFreeEbook
        isVisible={showModalFreeBook}
        setIsVisible={setShowModalFreeBook}
      />
    </Layout>
  );
}
