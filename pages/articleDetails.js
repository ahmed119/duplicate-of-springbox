import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";

import styles from "../styles/Story.module.css";
import ColorGuide from "../styles/colorGuide";

import Layout from "../components/layout";
import ArticlePreview from "../components/articlePreview.js";
import FollowNewsletter from "../components/followNewsletter.js";

import useWindowSize from "../hooks/useWindowSize";

import articlesList from "../data/articlesList";
import dataFAQ from "../data/dataFAQ";

export default function ArticleDetails() {
  
    useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);

  const {
    isTabletPortraitLayout,
    isMobileLayout,
    isSmallMobileLayout,
  } = useWindowSize();

  const {
    query: { articleID },
    push,
  } = useRouter();

  const article = articlesList[articleID];
  const { summaryList = [], paragraphList = [] } = article || {};

  const showFullScreenArticle = isTabletPortraitLayout || isMobileLayout;

  const DiscordInterlude = () => (
    <div
      className="containerSpaceBetween"
      style={{
        flexDirection: showFullScreenArticle ? "column" : "row",
        backgroundColor: ColorGuide.mainOrange,
        padding: showFullScreenArticle ? 30 : "4%",
        borderRadius: 30,
        marginTop: 30,
      }}
    >
      <div
        style={{
          width: showFullScreenArticle
            ? "100%"
            : showFullScreenArticle
            ? "80%"
            : "60%",
          marginBottom: showFullScreenArticle ? 30 : 0,
        }}
      >
        <h4>CONNECT</h4>
        <h3
          style={{
            fontFamily: "Work Sans",
            fontWeight: "700",
            fontSize: isMobileLayout ? 25 : 30,
          }}
        >
          <span className="underlineWhite">Chat</span> with other investors in
          our discord community
        </h3>
      </div>

      <div
        style={{
          width: showFullScreenArticle ? "100%" : "35%",
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-end",
          maxWidth: 250,
        }}
      >
        <img
          style={{
            alignSelf: "center",
            width: "80%",
            maxWidth: 150,
            height: "100%",
            marginBottom: showFullScreenArticle ? 0 : -20,
          }}
          src={"/art/joinOnDiscord.png"}
        />

        <button
          className="containerCenter shadowBlue"
          style={{
            alignSelf: "flex-end",
            cursor: "pointer",
            backgroundColor: ColorGuide.mainBlue,
            padding: 10,
            width: "100%",
            borderWidth: 0,
          }}
        >
          <p
            style={{
              color: ColorGuide.mainWhite,
              fontWeight: "600",
              marginRight: 10,
            }}
          >
            Join us on discord
          </p>

          <img
            style={{
              width: 20,
            }}
            src="/social/discordWhite.png"
            alt="Discord"
          />
        </button>
      </div>
    </div>
  );

  return (
    <Layout pageTitle="Thought Leadership">
      <div className="full_bloc">
        <div
          className="container"
          style={{ alignItems: "flex-start", paddingBottom: 0 }}
        >
          <Link href={"/thoughtLeadership"}>
            <p
              style={{
                position: "absolute",
                top: 30,
                cursor: "pointer",
                color: ColorGuide.mainOrange,
                fontWeight: "600",
                textDecoration: "underline",
              }}
            >
              Back to overview
            </p>
          </Link>
          <ArticlePreview
            {...article}
            articleID={articleID}
            useRowLayout
            reverse
            isPreview={false}
          />
        </div>
      </div>

      <div className="full_bloc">
        <div className="separator" />
      </div>

      <div className="full_bloc">
        <div
          className="container"
          style={{ alignItems: "flex-start", paddingBottom: 0, paddingTop: 50 }}
        >
          <div
            className={!showFullScreenArticle && "containerSpaceBetween"}
            style={{ alignItems: "flex-start" }}
          >
            <div style={{ width: showFullScreenArticle ? "100%" : "60%" }}>
              <p
                style={{
                  fontWeight: "600",
                  color: ColorGuide.mainBlue,
                  marginBottom: 20,
                }}
              >
                {summaryList[0]}
              </p>

              <p
                style={{
                  marginBottom: 20,
                }}
              >
                {summaryList[1]}
              </p>

              <div
                style={{
                  backgroundColor: ColorGuide.lightOrange,
                  padding: 20,
                  marginBottom: 30,
                }}
              >
                <p>We’ll be covering the following topics:</p>

                <ul>
                  {paragraphList.map(({ title }, index) => (
                    <a href={`#paragraph${index}`}>
                      <li style={{ color: ColorGuide.mainOrange }}>
                        <p
                          style={{
                            fontWeight: "600",
                            textDecoration: "underline",
                          }}
                        >
                          {title}
                        </p>
                      </li>
                    </a>
                  ))}
                </ul>
              </div>

              {paragraphList.map(({ title, content }, index) => (
                <div id={`paragraph${index}`} style={{ marginBottom: 30 }}>
                  <p style={{ fontWeight: "600", marginBottom: 20 }}>{title}</p>
                  {content()}
                  {index === 3 && <DiscordInterlude />}
                </div>
              ))}
            </div>

            <div style={{ width: showFullScreenArticle ? "100%" : "30%" }}>
              <p style={{ fontWeight: "600", marginBottom: 20 }}>
                Contributor:
              </p>

              <div className="containerRowMobile">
                <div
                  style={{
                    position: "relative",
                    width: 70,
                    height: 70,
                    marginRight: 20,
                    borderRadius: "50%",
                    borderWidth: 5,
                    borderColor: ColorGuide.mainOrange,
                    borderStyle: "solid",
                    overflow: "hidden",
                  }}
                >
                  <img
                    style={{
                      filter: "grayscale(100%)",
                      width: "100%",
                      height: "100%",
                    }}
                    src={"/team/fayze.png"}
                  />
                </div>

                <div>
                  <p>Fayze Bouaouid</p>
                  <p>CEO</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <FollowNewsletter />
    </Layout>
  );
}
