import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import Prismic from "prismic-javascript";
import { RichText, Date } from "prismic-reactjs";
import { default as NextLink } from "next/link";
import useWindowSize from "../hooks/useWindowSize";

import {
  Client,
  hrefResolver,
  linkResolver,
} from "../prismic-configuration.js";

import styles from "../styles/WhitePaper.module.css";
import ColorGuide from "../styles/colorGuide";

import Layout from "../components/layout";
import PostPreview from "../components/postPreview";


const Blog = ({ heroPosts, latestPosts }) => {
  useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);
  const { width: responsiveWidth, height: responsiveHeight } = useWindowSize();

  return (
    <Layout pageTitle="Blog">
      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainBlue }}
      >
        <div
          className="container"
          style={{ paddingTop: 50, paddingBottom: 150 }}
        >
          <h1 style={{ color: ColorGuide.mainWhite }}>Latest Blog Posts</h1>
        </div>
      </div>

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainWhite }}
      >
        <div className="container">
          <div
            className="containerSpaceBetween"
            style={{
              flexDirection: responsiveWidth < 650 && "column",
              marginTop: -240,
              marginBottom: responsiveWidth > 650 && 80,
              alignItems: "flex-start",
            }}
          >
            {heroPosts.map((post, index) => (
              <PostPreview key={post.uid} isHero={true} post={post} />
            ))}
          </div>

          <div
            className="containerSpaceBetween"
            style={{
              alignItems: "flex-start",
              flexDirection: responsiveWidth < 650 && "column",
            }}
          >
            {latestPosts.map((post, index) => (
              <PostPreview key={post.uid} isHero={false} post={post} />
            ))}
          </div>
        </div>
      </div>
    </Layout>
  );
};

Blog.getInitialProps = async (context) => {
  const { res } = context;

  if (res) {
    res.setHeader("Cache-Control", "s-maxage=1, stale-while-revalidate");
  }

  const temp = await Client().query(
    Prismic.Predicates.at("document.type", "blog_post"),
    { orderings: "[my.blog_post.postcreatedat desc]", pageSize: 5 }
  );

  const posts = temp.results;

  return { heroPosts: posts.splice(0, 2), latestPosts: posts.splice(0, 3) };
};

export default Blog;
