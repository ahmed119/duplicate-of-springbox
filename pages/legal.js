import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import styles from "../styles/pricing.module.css";
import ColorGuide from "../styles/colorGuide";

import useWindowSize from "../hooks/useWindowSize";

import Layout from "../components/layout";
import SubscribeQR from "../components/subscribeQR";
import SubscribeBubbles from "../components/subscribeBubbles";


export default function Subscribe() {

    useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);

  const { width, height } = useWindowSize();

  const [currentFAQIndex, setCurrentFAQIndex] = useState(0);

  const FAQListData = [
    {
      title:
        "Given my financial situation, should I choose an investment advisory service? Why or why not?",
      text:
        "Our service is designed for many types of investors.  However, using our service to invest in securities may not be appropriate for individuals who (i) do not have sufficient short-term savings to cover unexpected emergencies or (ii) have significant high interest debt, such as credit card debt.  We encourage such individuals to consider investing only after they have built up sufficient short-term savings and paid off most or all high interest debt. Investing using the services we provide is appropriate for persons who have built a financial cushion for emergencies and are able to comfortably pay off current debt.",
    },
    {
      title: "How will you choose investments to recommend to me?",
      text:
        "Our firm utilizes proprietary algorithms powered by artificial intelligence (“AI”) technology to generate trading signals on equity securities traded on the New York Stock Exchange, Nasdaq currencies traded on the foreign exchange market, and commodity futures. Our AI engine collects and analyzes large amounts of market data and then generates trading signals based on this analysis. The data analyzed by our intelligent system comes from multiple sources, both public and paid ones, such as media content, databases, posts and expert opinions. Our clients obtain access to our generated trading signals through a paid subscription service to our mobile application or web-based platform. Our trading signals are impersonal, meaning they are not tailored to any particular client. Rather, our trading signals are general market indicators that each of our clients can use as they choose.",
    },
    {
      title:
        "What is your relevant experience, including your licenses, education, and other qualifications? What do these qualifications mean?",
      text:
        "Our principal owner and CEO, Fayze Bouaouid, has a Master of Science in Banking and Finance. Fayze has nearly two decades of experience in the banking and asset management industries, and he brings a wealth of asset management knowledge to our firm.",
    },
    {
      title:
        "Help me understand how these fees and costs might affect my investments. If I give you $10,000 to invest, how much will go to fees and costs, and how much will be invested for me?",
      text:
        "Rather than deducting our fees, our firm exclusively offers a subscription service, which grants our clients access to our live trading forecast services. Our clients pay in advance for monthly or semi-annual access to our subscription service at the flat rates described in our firm brochure. Our firm does not charge any other types of fees for our services, such as custodian fees, mutual fund expenses, or any transaction or brokerage costs.  Clients choosing to trade securities must do so through a broker or agent not affiliated with our firm, and they will incur costs in making such trades.",
    },
    {
      title:
        "How might your conflicts of interest affect me, and how will you address them?",
      text:
        "Because of our fixed fee subscription service, we have designed our services for a very broad scope of investors. Not all signals may be applicable for your financial profile. That said, all signals are supported by the mechanics of our proprietary algorithms.",
    },
    {
      title:
        "As a financial professional, do you have a disciplinary history? For what type of conduct?",
      text: "None of our financial professionals have a disciplinary history.",
    },
    {
      title:
        "Who is my primary contact person? Is he or she a representative of an investment adviser? Who can I talk to if I have concerns about how this person is treating me?",
      text:
        "Our clients receive our services and are supported through the automated tools provided in our mobile application or web-based platform. Users who have administrative questions can review our website at www.springbox.ai or send us an email at info@springbox.tech.",
    },
  ];

  return (
    <Layout pageTitle="Legal">
      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.lightBlue }}
      >
        <div id="FAQ" className="container">
          <h1
            style={{
              color: ColorGuide.mainBlue,
              marginBottom: 40,
            }}
          >
            FORM CRS CONVERSATION STARTERS
          </h1>

          <h2
            style={{
              color: ColorGuide.mainBlue,
              marginBottom: 40,
            }}
          >
            Conversation Starters
          </h2>

          {FAQListData.map(({ title, text }, index) => (
            <>
              <div
                onClick={() =>
                  setCurrentFAQIndex(currentFAQIndex === index ? null : index)
                }
                className="containerSpaceBetween"
                style={{ marginBottom: 20, cursor: "pointer" }}
              >
                <h3
                  className={`${styles.titleFAQ} ${
                    currentFAQIndex === index && styles.currentTitleFAQ
                  }`}
                  style={{
                    width: "90%",
                    fontSize: width < 650 ? 15 : 20,
                  }}
                >
                  {title}
                </h3>

                <img
                  style={{
                    height: 20,
                    width: 20,
                    transition: "1s",
                    transform: currentFAQIndex !== index && "rotate(2700deg)",
                  }}
                  src={"/UI/arrow.png"}
                />
              </div>

              {currentFAQIndex === index && (
                <>
                  <p style={{ color: ColorGuide.mainBlue }}>{text}</p>

                  <div
                    style={{
                      width: "100%",
                      height: 2,
                      backgroundColor: ColorGuide.mainBlue,
                      marginTop: 40,
                      marginBottom: 40,
                    }}
                  />
                </>
              )}
            </>
          ))}
        </div>
      </div>
    </Layout>
  );
}
