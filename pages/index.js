import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import Link from "next/link";
import Lottie from "react-lottie";

import Layout from "../components/layout";
import SliderContainer from "../components/sliderContainer";
import SpringboxPerformance from "../components/springboxPerformance";

import useWindowSize from "../hooks/useWindowSize";

import styles from "../styles/Home.module.css";
import ColorGuide from "../styles/colorGuide";

import SmartImage from "../components/smartImage";
import AppInformations from "../components/appInformations";
import DownloadApp from "../components/downloadApp";

import TagManager from 'react-gtm-module'


export default function Home() {

  useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);

  const {
    width: responsiveWidth,
    height: responsiveHeight,
    isSmallMobileLayout,
    isMobileLayout,
    isTabletLandscapeLayout,
    isTabletPortraitLayout,
    isPortraitLayout,
  } = useWindowSize();

  
  const [currentSection, setCurrentSection] = useState(null);
  const [showVideo, setShowVideo] = useState(false);

  const dataFanFeedbacks = [
    {
      storeLogo: "/UI/googlePlayReview.png",
      text:
        "This is legit and I'm loving it. Thank you. For those of you saying some have gone down, duh not everything is a winner. Trading is a game of volatility. Do your homework and choose the tickers that call out to you and seem promising. Act quick cut your losses quickly and stack it up.",
      userName: "Will Dyer",
      date: "February 19, 2021",
    },
    {
      storeLogo: "/UI/appStoreReview.png",
      text:
        "This app works if you know what you’re doing, it doesn’t trade for you and does not tells you how to trade, YOU as a trader has to do your due diligence. This app works when you look at a chart to see if the alert you’re being given is in sync with the trend if the stock. It it worth the price? Of course it is, the only draw back that I see is that most times the forecast does not alert when you should get out of a play because sometimes they end up closing a trade before target price, like I said, so your own homework. Have I made money? Yes, it’s a matter of not getting greedy or thinking that you put money and you’ll make millions, doesn’t work like that.",
      userName: "G5iv3",
      date: "Jun 21, 2021",
    },
  ];

  const dataHowToUse = [
    {
      title: "View Springbox stock recommendation",
      text:
        "The AI scans and recommend the expected winners, in this case, UBER is a recommended Buy.",
      image: "/mockups/howToUse/viewStockRecommendation.png",
    },
    {
      title: "View Stock overview",
      text:
        "Springbox AI provides you with all the relevant market indicators regarding the recommended stock. All the relevant information is available at a glance, and is easy to read and understand.",
      image: "/mockups/howToUse/viewStockOverview.png",
    },
    {
      title: "Follow recommendation",
      text:
        "The app uses advanced AI algorithms to provide trading signals and recommend actions regarding your chosen stock. In this instance, it recommends you log the trade in your broker app and buy the Uber shares.",
      image: "/mockups/howToUse/followRecommendation.png",
    },
    {
      title: "Explore the metrics",
      text:
        "Further information is provided to app users including vital information like target price (TP), stop loss (SL) and expected profit and profitability metrics.",
      image: "/mockups/howToUse/exploreMetrics.png",
    },
  ];

  const dataPresentation = [
    {
      question: "AI powered",
      title: "A daily trading plan",
      text:
        "Springbox AI is the proprietary platform designed to assist you in your daily trading plan with live AI-powered signals.",
      subText:
        "Our goal is to give you the opportunity to make consistent and regular profits with clarity and increased accuracy.",
    },
    {
      question: "Connect",
      title: "Our Discord community",
      text:
        "All users gain access to our exclusive community server on Discord. This is the place where you connect with other uers, chit chat about everything investements related, get access to cool podcasts and clubhouse drop ins, let’s connect!",
    },
  ];

  const dataIntroduction = [
    {
      question: "how do we do it?",
      title: "The Algorithm",
      text:
        "To do that we use advanced algorithms able to identify high market patterns profitability, so that you can take full advantage of any trading opportunity.",
      subText:
        "It’s the same AI technology used at institutional level, now made available to all investors. It’s forecasting analysis, done in an intelligent, cost-effective & democratic manner.",
    },
    {
      question: "what you get",
      title: "The Dashboard",
      text:
        "You get access to our financial dashboard for trading forecasts, live markets data, analysis and assistance, set your watchlist and get the notifications that matter to you.",
      subText:
        "This means that whether you trade in stocks, forex, commodities or ETFs, we bring clarity and real-time insight to your potential choices.",
    },
  ];

  const dataWhyChoose = [
    {
      title: (
        <h2>
          Markets
          <br />
          <span className="underlineBlue">Forecasting</span>
        </h2>
      ),
      text:
        "Clear buy and sell signals, transparent and in real-time, ensure you never miss an opportunity to make profits. Receive mobile notifications prompting you to take action, get busy earning.",
      image: "/mockups/whyChoose/marketForecasting.png",
    },
    {
      title: (
        <h2>
          <span className="underlineBlue">Smart</span>
          <br />
          Signals
        </h2>
      ),
      text:
        "The stock market can be volatile, with Springbox smart signals, we ensure you are able to leverage the volatility for maximum profit.",
      image: "/mockups/whyChoose/smartSignals.png",
    },
    {
      title: (
        <h2>
          <span className="underlineBlue">Live</span> Market
          <br />
          Screening
        </h2>
      ),
      text:
        "Market patterns profitability at your fingertips! Our AI scans the top 10 assets to buy and sell at all times, so you don't have to, simple hey?",
      image: "/mockups/whyChoose/marketScreening.png",
    },
    {
      title: (
        <h2>
          A <span className="underlineBlue">Democratic</span>
          <br />
          Approach
        </h2>
      ),
      text:
        "At Springbox we believe you deserve access to the same tech and next-generation AI used by big corporations, we are all for decentralisation and community, we are better together.",
      image: "/mockups/whyChoose/democraticApproach.png",
    },
    {
      title: (
        <h2>
          High <span className="underlineBlue">Accuracy</span>
        </h2>
      ),
      text:
        "Our goal is to bring transparency, predictability, and accuracy to your trading game, resulting in more profit, more frequently.",
      image: "/mockups/whyChoose/highAccuracy.png",
    },
    {
      title: (
        <h2>
          <span className="underlineBlue">User-Friendly</span>
          <br />
          Interface
        </h2>
      ),
      text:
        "The world of investments is complicated enough, our app does not need to be. A clean, easy to understand 4 step process that ensures you trade profitably.",
      image: "/mockups/whyChoose/userFriendly.png",
    },
  ];

  const dataPress = [
    {
      width: 80,
      link:
        "https://techcrunch.com/2021/01/21/financial-forecasting-startup-springbox-ai-launches-its-apps-and-raises-2m/",
      logo: "/press/techCrunch.png",
      text:
        "Most brokers focus their marketing by selling investors the dream or the myth of ...",
    },
    {
      width: 150,
      link:
        "https://www.forbes.com/sites/curtissilver/2021/01/21/springbox-ai-bolsters-your-trades-with-its-affordable-financial-insights-app/?sh=30b7409c3be6",
      logo: "/press/forbes.png",
      text:
        "Springbox AI Bolsters Your Trades With Its Affordable Financial Insights App.",
    },
    {
      width: 140,
      link:
        "https://finance.yahoo.com/news/financial-forecasting-startup-springbox-ai-110337334.html",
      logo: "/press/yahooFinance.png",
      text:
        "Financial forecasting startup Springbox AI launches its apps and raises $2M.",
    },
    {
      width: 180,
      link:
        "https://www.nasdaq.com/articles/how-the-next-generation-of-investors-are-gamifying-investing-2020-12-17",
      logo: "/press/nasdaq.png",
      text: "How the Next Generation of Investors are Gamifying Investing",
    },
  ];

  const decorationItemSize = isMobileLayout ? 80 : 140;

  return (
    <Layout pageTitle="What stock to buy?">
      <div className="full_bloc" style={{ backgroundColor: "#0024D3" }}>
        <div id={styles.heroWoman} className="containerCenter">
          <img
            className={!isMobileLayout && "imageFit"}
            src={
              isMobileLayout
                ? "/pictures/heroWomanMobile.png"
                : "/pictures/heroWoman.png"
            }
          />
          {!isMobileLayout && (
            <div id={styles.playButton} onClick={() => setShowVideo(true)}>
              <img
                style={{ width: 100, height: 100, marginBottom: 10 }}
                src="/UI/playCircle.png"
              />

              <h4
                style={{
                  color: ColorGuide.mainWhite,
                  fontWeight: "600",
                  textAlign: "center",
                }}
              >
                Watch Video
              </h4>
            </div>
          )}
        </div>

        {!isMobileLayout && (
          <img
            style={{
              position: "absolute",
              bottom: 50,
              left: "45%",
              width: 300,
            }}
            src="/art/circleThin.png"
            alt="stock market forecast"
          />
        )}

        <div id={styles.containerHero} className="container">
          <div>
            <img
              style={{
                maxWidth: 100,
                marginBottom: 30,
              }}
              src="/art/heroCircle.png"
              alt="stock market forecast"
            />

            <div
              style={{
                width: isMobileLayout
                  ? "65%"
                  : isTabletLandscapeLayout
                  ? "50%"
                  : "100%",
              }}
            >
              <h1
                style={{
                  color: ColorGuide.mainWhite,
                  marginBottom: 20,
                }}
              >
                Smarter Trading
                <br />
                Decisions
              </h1>

              <h2
                style={{
                  color: ColorGuide.mainWhite,
                  marginBottom: 30,
                }}
              >
                AI-driven signals, for profitable trading
              </h2>

              {isMobileLayout && (
                <button
                  onClick={() => setShowVideo(true)}
                  className="containerCenter"
                  style={{
                    width: isMobileLayout ? 230 : 250,
                    cursor: "pointer",
                    backgroundColor: "transparent",
                    padding: isMobileLayout ? 10 : 15,
                    paddingRight: 30,
                    paddingLeft: 30,
                    borderWidth: 1,
                    borderColor: ColorGuide.mainWhite,
                    marginBottom: 20,
                  }}
                >
                  <img
                    src="/UI/play.png"
                    style={{ width: 13, marginRight: 10 }}
                    alt="start trading"
                  />

                  <p
                    style={{
                      color: ColorGuide.mainWhite,
                      fontWeight: "600",
                      marginRight: 5,
                    }}
                  >
                    Watch Video
                  </p>
                </button>
              )}

              <DownloadApp />
            </div>
          </div>
        </div>
      </div>

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainWhite }}
      >
        <div className="container" style={{ alignItems: "center" }}>
          <div
            className="containerSpaceBetween"
            style={{
              width: isMobileLayout
                ? "100%"
                : isTabletPortraitLayout
                ? "80%"
                : "60%",
              alignSelf: "center",
              margin: "0 auto",
              marginBottom: 50,
            }}
          >
            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/feedbackFansLeft.png"
              alt="stock market forecast"
            />

            <div>
              <h4 style={{ textAlign: "center" }}>REVIEWS</h4>
              <h2
                style={{
                  margin: "0 auto",
                  textAlign: "center",
                  width: isMobileLayout ? "80%" : "100%",
                }}
              >
                Feedback from our fans
              </h2>
            </div>

            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/feedbackFansRight.png"
              alt="stock market forecast"
            />
          </div>

          <SliderContainer
            data={dataFanFeedbacks}
            desktopClass={"containerRow"}
            sliderContainerStyle={{ width: responsiveWidth * 0.9 }}
            renderInitial={({ storeLogo, text, userName, date }) => (
              <div
                style={{
                  width: isMobileLayout ? responsiveWidth * 0.8 : "50%",
                }}
              >
                <div
                  className="containerRowMobile"
                  style={{ marginBottom: 15 }}
                >
                  <img
                    style={{
                      maxWidth: 100,
                      marginRight: 30,
                    }}
                    src="/UI/fiveStars.png"
                    alt="stock market forecast"
                  />

                  <img
                    style={{
                      height: 25,
                    }}
                    src={storeLogo}
                    alt="stock market forecast"
                  />
                </div>

                <p style={{ marginBottom: 15 }}>{text}</p>

                <div className="containerRowMobile">
                  <p style={{ fontWeight: "600", marginRight: 30 }}>
                    {userName}
                  </p>
                  <p style={{ color: ColorGuide.mainBlue }}>{date}</p>
                </div>
              </div>
            )}
          />
        </div>
      </div>

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainGreen }}
      >
        <div className="container">
          <h4
            style={{
              color: ColorGuide.mainWhite,
              textAlign: "center",
            }}
          >
            FEATURED IN
          </h4>

          <h2
            style={{
              color: ColorGuide.mainWhite,
              textAlign: "center",
            }}
          >
            Word on the street
          </h2>

          <SliderContainer
            data={dataPress}
            desktopClass={"containerSpaceBetween"}
            renderInitial={({ link, width, logo, text }) => (
              <div
                style={{
                  width: isSmallMobileLayout
                    ? responsiveWidth * 0.7
                    : isMobileLayout
                    ? responsiveWidth * 0.8
                    : "auto",
                }}
              >
                <div style={{ height: 80 }}>
                  <img
                    src={logo}
                    style={{
                      marginBottom: 20,
                      maxWidth: width,
                      width: isSmallMobileLayout ? "60%" : "80%",
                    }}
                  />
                </div>
                <p
                  style={{
                    color: ColorGuide.mainWhite,
                    marginBottom: 20,
                    width: "80%",
                  }}
                >
                  “{text}”
                </p>
                <Link href={link}>
                  <p
                    style={{
                      cursor: "pointer",
                      fontWeight: "600",
                      color: ColorGuide.mainOrange,
                      textDecoration: "underline",
                    }}
                  >
                    Read article
                  </p>
                </Link>
              </div>
            )}
          />
        </div>
      </div>

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainOrange }}
      >
        <div className="container" style={{ position: "relative" }}>
          <h4 style={{ textAlign: "center" }}>4 STEPS</h4>
          <h2 style={{ textAlign: "center" }}>How to use Springbox AI</h2>

          {!isMobileLayout && (
            <>
              <img
                style={{
                  zIndex: -1,
                  position: "absolute",
                  top: 60,
                  right: 100,
                  width: 300,
                }}
                src="/art/doubleCircleThinMulti.png"
                alt="stock market forecast"
              />

              <img
                style={{
                  zIndex: -1,
                  position: "absolute",
                  bottom: 120,
                  left: 100,
                  width: 120,
                }}
                src="/art/circleYellowThin.png"
                alt="stock market forecast"
              />
            </>
          )}

          <SliderContainer
            data={dataHowToUse}
            desktopClass={"containerWrap"}
            sliderContainerStyle={{
              padding: 20,
              width: isSmallMobileLayout ? 280 : 350,
            }}
            renderInitial={({ title, text, image }, index) => (
              <div
                className={`itemWrap shadowOrange`}
                style={{
                  flexDirection: isPortraitLayout ? "column" : "row",
                  width: isSmallMobileLayout
                    ? "75%"
                    : isMobileLayout
                    ? "80%"
                    : "40%",
                  height: isSmallMobileLayout
                    ? 350
                    : isMobileLayout
                    ? 400
                    : "auto",
                  backgroundColor: ColorGuide.mainWhite,
                  alignItems: "center",
                  justifyContent: "flex-start",
                  padding: isMobileLayout ? 30 : "4%",
                  margin: "1%",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginBottom: isPortraitLayout ? 30 : 0,
                  }}
                >
                  <img
                    style={{
                      maxHeight: isMobileLayout ? 150 : 220,
                      width: isPortraitLayout ? "50%" : "40%",
                      alignSelf: isPortraitLayout ? "flex-end" : "center",
                    }}
                    src={image}
                    alt="How to use Springbox"
                  />

                  <div
                    style={{
                      display: "flex",
                      flexDirection: isPortraitLayout ? "column" : "row",
                      marginLeft: -15,
                    }}
                  >
                    <h1
                      style={{
                        fontFamily: "Work Sans",
                        marginTop: 30,
                        marginRight: 20,
                        color: ColorGuide.mainBlue,
                        opacity: 0.1,
                        fontSize: isSmallMobileLayout ? 45 : 65,
                      }}
                    >
                      {index + 1}.
                    </h1>

                    <div>
                      <h3
                        style={{
                          fontFamily: "Work Sans",
                          fontWeight: "600",
                          color: ColorGuide.mainBlue,
                          marginBottom: 10,
                          width: isPortraitLayout
                            ? "100%"
                            : isSmallMobileLayout
                            ? "70%"
                            : "95%",
                        }}
                      >
                        {title}
                      </h3>
                      {!isPortraitLayout && <p>{text}</p>}
                    </div>
                  </div>
                </div>

                {isPortraitLayout && <p style={{ width: "100%" }}>{text}</p>}
              </div>
            )}
          />
        </div>
      </div>

      {dataPresentation.map(({ question, title, text, subText }, index) => (
        <div className="full_bloc">
          <div
            key={index}
            className={`container containerRow ${
              styles.containerSectionFeatures
            } fullWidthMobile ${index === 0 && "reverseMobile"}`}
          >
            {index === 1 && (
              <div className="halfBloc containerIllustration containerCenter">
                <img
                  style={{
                    width: "90%",
                  }}
                  src="/mockups/discord.gif"
                  alt="trade dashboard"
                />
              </div>
            )}

            <div
              className={`halfBloc alternateBloc containerMobile`}
              style={{
                position: "relative",
                paddingLeft: responsiveWidth > 650 && index === 1 && 0,
              }}
            >
              {index === 1 && (
                <img
                  style={{
                    zIndex: -1,
                    position: "absolute",
                    top: 30,
                    left: -30,
                    width: 300,
                  }}
                  src="/art/springboxBubbles.png"
                  alt="stock market forecast"
                />
              )}

              <h4 style={{ marginBottom: 10 }}>{question.toUpperCase()}</h4>

              <h2 style={{ marginBottom: 30 }}>{title}</h2>

              <p style={{ marginBottom: 10 }}>{text}</p>

              <p
                style={{
                  marginBottom: 30,
                }}
              >
                {subText}
              </p>

              <Link href={"/pricing"}>
                <button
                  className="containerCenter"
                  style={
                    index === 0
                      ? {
                          backgroundColor: ColorGuide.mainBlue,
                          borderWidth: 0,
                        }
                      : { borderColor: ColorGuide.mainBlue }
                  }
                >
                  <p
                    style={{
                      color:
                        index === 0
                          ? ColorGuide.mainWhite
                          : ColorGuide.mainBlue,
                    }}
                  >
                    {index === 1 ? "Let's Connect" : "Get Started"}
                  </p>

                  {index === 1 ? (
                    <img
                      style={{
                        marginLeft: 5,
                        width: 40,
                      }}
                      src="/social/discordBlue.png"
                      alt="stock market forecast"
                    />
                  ) : (
                    <img
                      style={{ width: 12, marginLeft: 10 }}
                      src="/UI/chevronWhite.png"
                      alt="Springbox AI Newsletter"
                    />
                  )}
                </button>
              </Link>
            </div>

            {index === 0 && (
              <div
                className="halfBloc containerIllustration containerCenter"
                style={{ alignItems: "center" }}
              >
                <img
                  style={{
                    width: "80%",
                    maxWidth: 450,
                  }}
                  src="/art/dailyTradingPlan.png"
                  alt="stock market forecast"
                />
              </div>
            )}
          </div>
        </div>
      ))}

      <SpringboxPerformance />

      {dataIntroduction.map(({ question, title, text, subText }, index) => (
        <div className="full_bloc">
          <div
            key={index}
            className={`container containerRow ${
              styles.containerSectionFeatures
            } fullWidthMobile ${index === 0 && "reverseMobile"}`}
          >
            {index === 1 && (
              <div className="halfBloc containerIllustration containerCenter">
                <img
                  style={{
                    width: isTabletLandscapeLayout ? "85%" : 450,
                  }}
                  src="/mockups/trade-dashboard.png"
                  alt="trade dashboard"
                />
              </div>
            )}

            <div
              className={`halfBloc alternateBloc containerMobile`}
              style={{
                position: "relative",
                paddingLeft: !isMobileLayout && index === 1 && 0,
              }}
            >
              {index === 1 && (
                <img
                  style={{
                    zIndex: -1,
                    position: "absolute",
                    top: 30,
                    left: -30,
                    width: 300,
                  }}
                  src="/art/springboxBubbles.png"
                  alt="stock market forecast"
                />
              )}

              <h4 style={{ marginBottom: 10 }}>{question.toUpperCase()}</h4>

              <h2 style={{ marginBottom: 30 }}>{title}</h2>

              <p style={{ marginBottom: 10 }}>{text}</p>

              <p
                style={{
                  marginBottom: 30,
                }}
              >
                {subText}
              </p>

              <Link href={index === 0 ? "/pricing#FAQ" : "/pricing"}>
                <button
                  className="containerCenter"
                  style={
                    index === 1
                      ? {
                          backgroundColor: ColorGuide.mainBlue,
                          borderWidth: 0,
                        }
                      : { borderColor: ColorGuide.mainBlue }
                  }
                >
                  <p
                    style={{
                      color:
                        index === 1
                          ? ColorGuide.mainWhite
                          : ColorGuide.mainBlue,
                    }}
                  >
                    {index === 0 ? "Learn more" : "Get Started"}
                  </p>

                  {index === 1 && (
                    <img
                      style={{ width: 12, marginLeft: 10 }}
                      src="/UI/chevronWhite.png"
                      alt="Springbox AI Newsletter"
                    />
                  )}
                </button>
              </Link>
            </div>

            {index === 0 && (
              <div
                className="halfBloc containerIllustration containerCenter"
                style={{
                  alignItems: "center",
                  ...(isMobileLayout && { height: 350 }),
                }}
              >
                <img
                  style={{
                    width: "80%",
                    maxWidth: 450,
                  }}
                  src="/art/stock-market-forecast.png"
                  alt="stock market forecast"
                />
              </div>
            )}
          </div>
        </div>
      ))}

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainGreen }}
      >
        <div
          className="container containerCenter"
          style={{ flexDirection: "column" }}
        >
          <h2
            style={{
              textAlign: "center",
              color: "white",
              width: isMobileLayout ? "90%" : "70%",
              margin: "0 auto",
              marginBottom: 30,
            }}
          >
            Get unlimited access and start making profits at a democratic price.{" "}
            <span style={{ color: ColorGuide.mainOrange }}>
              Cancel anytime.
            </span>
          </h2>

          <Link href={"/pricing"}>
            <button
              className="containerCenter"
              style={{ borderColor: ColorGuide.mainWhite }}
            >
              <p style={{ color: ColorGuide.mainWhite }}>Learn more</p>
            </button>
          </Link>
        </div>
      </div>

      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainWhite }}
      >
        <div className="container" style={{ position: "relative" }}>
          <div
            className="containerSpaceBetween"
            style={{
              width: isMobileLayout
                ? "100%"
                : isTabletPortraitLayout
                ? "80%"
                : "60%",
              alignSelf: "center",
              margin: "0 auto",
              marginBottom: 50,
            }}
          >
            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/whyChooseLeft.png"
              alt="stock market forecast"
            />

            <div>
              <h4 style={{ textAlign: "center" }}>WHY US?</h4>
              <h2
                style={{
                  margin: "0 auto",
                  textAlign: "center",
                  width: isMobileLayout ? "80%" : "100%",
                }}
              >
                Why choose Springbox AI
              </h2>
            </div>

            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/whyChooseRight.png"
              alt="stock market forecast"
            />
          </div>

          {!isMobileLayout && (
            <>
              <img
                style={{
                  zIndex: -1,
                  position: "absolute",
                  top: 600,
                  right: -30,
                  width: 300,
                  transform: "rotate(180deg)",
                }}
                src="/art/springboxBubbles.png"
                alt="stock market forecast"
              />

              <img
                style={{
                  zIndex: -1,
                  position: "absolute",
                  bottom: 300,
                  left: -120,
                  width: 300,
                  transform: "rotate(90deg)",
                }}
                src="/art/springboxBubbles.png"
                alt="stock market forecast"
              />

              <img
                style={{
                  zIndex: -1,
                  position: "absolute",
                  top: 350,
                  left: -100,
                  width: 300,
                }}
                src="/art/doubleCircleThinBlueLight.png"
                alt="stock market forecast"
              />

              <img
                style={{
                  zIndex: -1,
                  position: "absolute",
                  bottom: 200,
                  right: -100,
                  width: 300,
                }}
                src="/art/doubleCircleThinBlueLightAlternate.png"
                alt="stock market forecast"
              />
            </>
          )}

          <SliderContainer
            data={dataWhyChoose}
            desktopClass={"containerWrap"}
            sliderContainerStyle={{
              marginRight: 30,
              width: isMobileLayout ? 300 : "auto",
            }}
            renderInitial={({ title, text, image, underline }, index) => (
              <div
                className={`itemWrap shadowBlack`}
                style={{
                  width: isSmallMobileLayout
                    ? 210
                    : isMobileLayout
                    ? 210
                    : isTabletPortraitLayout
                    ? "40%"
                    : "23%",
                  flexDirection: "column",
                  backgroundColor: ColorGuide.lightBlue,
                  justifyContent: "flex-start",
                  alignItems: "flex-start",
                  paddingTop: 80,
                  ...(isMobileLayout
                    ? { padding: 30, marginRight: 30, height: 400 }
                    : {}),
                }}
              >
                <div
                  className="containerCenter"
                  style={{
                    alignSelf: "flex-start",
                    backgroundColor: ColorGuide.lightOrange,
                    width: 60,
                    height: 60,
                    borderRadius: 50,
                    marginBottom: 20,
                  }}
                >
                  <img
                    style={{
                      width: "40%",
                      height: "40%",
                    }}
                    src={image}
                    alt="How to use Springbox"
                  />
                </div>

                <div>
                  <h2
                    style={{
                      color: ColorGuide.mainBlack,
                      fontWeight: "600",
                      height: isMobileLayout ? 30 : 50,
                    }}
                  >
                    {title}
                  </h2>
                  <p style={{ color: ColorGuide.mainBlack }}>{text}</p>
                </div>
              </div>
            )}
          />
        </div>
      </div>

      {showVideo && (
        <div className="containerCenter containerModal">
          <video
            style={{ width: "80%", height: "50%" }}
            autoPlay
            controls
            playsInline
            src="/videos/introduction.mp4"
          />

          <img
            onClick={() => setShowVideo(false)}
            className="iconDefault"
            style={{
              position: "absolute",
              top: 150,
              right: 100,
              cursor: "pointer",
            }}
            src="/UI/closeWhite.png"
          />
        </div>
      )}
    </Layout>
  );
}
