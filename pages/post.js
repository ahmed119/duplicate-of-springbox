import React, { useState } from "react";
import { useEffect } from "react";
import Head from "next/head";
import { RichText, Date } from "prismic-reactjs";
import Prismic from "prismic-javascript";

import Layout from "../components/layout";
import PostPreview from "../components/postPreview";
import RenderText from "../components/renderText";

import useWindowSize from "../hooks/useWindowSize";

import ColorGuide from "../styles/colorGuide";

import { Client } from "../prismic-configuration.js";

const dateFormat = {
  month: "long",
  day: "2-digit",
  year: "numeric",
};

const Post = ({ post, latestPosts }) => {
  useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);
  const { width: responsiveWidth, height: responsiveHeight } = useWindowSize();

  if (post && post.data) {
    const {
      postheroimage,
      posttitle,
      postauthor,
      postcreatedat,
      posttext,
    } = post.data;

    return (
      <Layout pageTitle={RichText.asText(posttitle)}>
        <div
          className="full_bloc"
          style={{ backgroundColor: ColorGuide.lightBlue, overflow: "hidden" }}
        >
          <div className="container">
            <h1
              style={{
                color: ColorGuide.mainBlue,
                width: responsiveWidth < 650 ? "80%" : "40%",
              }}
            >
              {RichText.asText(posttitle)}
            </h1>

            <img
              className="imageFit"
              style={{
                width: "100%",
                height: 250,
                borderRadius: 20,
                marginBottom: 30,
              }}
              src={postheroimage.url}
            />

            <p style={{ color: ColorGuide.mainBlue }}>By {postauthor}</p>

            <p
              style={{
                color: ColorGuide.mainOrange,
                textDecoration: "underline",
              }}
            >
              {new Intl.DateTimeFormat("en-US", dateFormat).format(
                Date(postcreatedat)
              )}
            </p>
          </div>
        </div>
        <div
          className="full_bloc"
          style={{ backgroundColor: ColorGuide.mainWhite }}
        >
          <div className="container" style={{ paddingBottom: 0 }}>
            <h3
              style={{
                color: ColorGuide.mainBlue,
                width: responsiveWidth < 650 ? "80%" : "30%",
                marginBottom: 30,
              }}
            >
              {RichText.asText(posttitle)}
            </h3>

            <RenderText data={posttext} />

            <p style={{ color: ColorGuide.mainBlack, fontStyle: "italic" }}>
              By {postauthor}
            </p>
          </div>

          <div className="container">
            <h1 style={{ color: ColorGuide.mainBlue }}>Latest Blog Post</h1>

            <div
              className="containerSpaceBetween"
              style={{
                alignItems: "flex-start",
                flexDirection: responsiveWidth < 650 && "column",
              }}
            >
              {latestPosts.map((post, index) => (
                <PostPreview key={post.uid} isHero={false} post={post} />
              ))}
            </div>
          </div>
        </div>
      </Layout>
    );
  }

  return null;
};

Post.getInitialProps = async (context) => {
  const { res, query, preview = null, previewData = {} } = context;

  if (res) {
    res.setHeader("Cache-Control", "s-maxage=1, stale-while-revalidate");
  }

  const { uid } = query;
  const { ref } = previewData;

  const post = await Client().getByUID("blog_post", uid);

  const latestPosts = await Client().query(
    Prismic.Predicates.at("document.type", "blog_post"),
    {
      orderings: "[my.blog_post.postcreatedat desc]",
      pageSize: 3,
      ...(ref ? { ref } : null),
    }
  );

  return {
    post,
    latestPosts: latestPosts ? latestPosts.results : [],
  };
};

export default Post;
