import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import Link from "next/link";

import styles from "../styles/Story.module.css";
import ColorGuide from "../styles/colorGuide";

import Layout from "../components/layout";
import ArticlePreview from "../components/articlePreview.js";

import useWindowSize from "../hooks/useWindowSize";

import articlesList from "../data/articlesList";
import dataFAQ from "../data/dataFAQ";

export default function Articles() {
  
  useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);
  const { isMobileLayout, isTabletPortraitLayout } = useWindowSize();

  const ebookList = [
    { image: "/books/profitableSideHustle.png", downloadLink: "#" },
    { image: "/books/addingArtificialIntelligence.png", downloadLink: "#" },
    { image: "/books/comingSoon.png", downloadLink: "#" },
  ];

  const decorationItemSize = isMobileLayout ? 60 : 140;

  return (
    <Layout pageTitle="Thought Leadership">
      <div className="full_bloc">
        <div
          className="container"
          style={{ position: "relative", paddingBottom: 0 }}
        >
          <Link href={"/thoughtLeadership"}>
            <p
              style={{
                position: "absolute",
                top: 30,
                cursor: "pointer",
                color: ColorGuide.mainOrange,
                fontWeight: "600",
                textDecoration: "underline",
              }}
            >
              Back to overview
            </p>
          </Link>

          <img
            style={{
              position: "absolute",
              top: isMobileLayout ? 20 : 40,
              right: isMobileLayout ? "25%" : "35%",
              width: 60,
            }}
            src="/art/accordeon.png"
            alt="stock market forecast"
          />

          <img
            style={{
              position: "absolute",
              top: isMobileLayout ? 100 : 80,
              left: isMobileLayout ? "10%" : "32%",
              width: 60,
              transform: "rotate(90deg)",
            }}
            src="/art/accordeon.png"
            alt="stock market forecast"
          />

          <div
            className="containerSpaceBetween"
            style={{
              width: isMobileLayout
                ? "100%"
                : isTabletPortraitLayout
                ? "80%"
                : "60%",
              alignSelf: "center",
              margin: "0 auto",
            }}
          >
            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/theTeamLeft.png"
              alt="stock market forecast"
            />

            <h1
              style={{
                textAlign: "center",
              }}
            >
              Articles
            </h1>

            <img
              style={{
                width: decorationItemSize,
                marginBottom: 15,
              }}
              src="/art/theTeamRight.png"
              alt="stock market forecast"
            />
          </div>
        </div>
      </div>

      <div className="full_bloc">
        <div className="container" style={{ position: "relative" }}>
          {articlesList.map((article, articleID) => (
            <ArticlePreview {...article} articleID={articleID} useRowLayout />
          ))}
        </div>
      </div>
    </Layout>
  );
}
