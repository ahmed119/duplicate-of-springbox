import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import styles from "../styles/pricing.module.css";
import ColorGuide from "../styles/colorGuide";

import useWindowSize from "../hooks/useWindowSize";

import Layout from "../components/layout";


export default function Subscribe() {

  useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);
  const { width: responsiveWidth, height: responsiveHeight } = useWindowSize();
  const [currentFAQIndex, setCurrentFAQIndex] = useState(0);

  return (
    <Layout className={styles.container} pageTitle={"Privacy Policy"}>
      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainWhite }}
      >
        <div className="container">
          <h1
            style={{
              color: ColorGuide.mainBlue,
              marginBottom: 40,
            }}
          >
            Privacy Policy
          </h1>

          <p style={{ color: ColorGuide.mainBlue }}>
            1. OVERVIEW OF THIS POLICY <br />
            <br />
            This Privacy Policy (this “Policy”) relates to the personal
            information that you submit when you access and use our Website or
            App and other information we automatically collect when you access
            the Website or App. This Policy explains all activities regarding
            your information, including but not limited to how we collect, use,
            disclose, and secure it and the rights available to you. You are
            required to read, understand, and agree to how we process
            (collection, use, disclosure, security, etc.) your information in
            this Policy. You are also required to read our Terms and Conditions
            and print a copy each for future reference. By your use of the
            Website or App and your submission of the requested personal
            information, you hereby agree to how we process your information as
            described in this Policy. if you do not agree, you should not submit
            information. <br />
            <br />
            2. WHAT INFORMATION DOES SPRINGBOX AI COLLECT FROM YOU? <br />
            <br />
            2.1 We collect information from and about users of our Website or
            App: • Directly from you when you provide it to us. • Automatically
            when you use the Website or App. <br />
            <br />
            2.2 We do not collect special categories of personal data such as
            information relating to or about the below or information about
            criminal records. • Health • Racial or ethnic origin • Political
            opinions • Membership of a political association, professional or
            trade association • Membership of a trade union • Religious beliefs
            or affiliations • Philosophical beliefs • Sexual orientation or
            practices • Biometric information 2.3 Information that you provide
            to us (“personal information”) when you register to use our Website
            or App or App may include: • First Name, Last Name; • Email Address;
            • Banking information, credit card, and debit card number 2.4 This
            information may also include: • Information that you provide by
            registering on the Website or App or App such as first name, last
            name, email address, telephone number and address (“Contact
            Information”). This includes information provided at the time of
            registering to use the Website or App and requesting further
            services. • Information you provide to us when you complete user
            surveys or surveys for research purposes such as your Contact
            Information. • Information you provide to us when you report a
            problem with the Website or App such as your first name, last name
            and email address. • Records and copies of your correspondence
            (including e-mail addresses and phone numbers), if you contact us. •
            Details of transactions you carry out through the Website or App
            when you make payment to Springbox Ai. • Your correspondence within
            the Website or App with other users (your username and location). 3.
            WHAT DOES SPRINGBOX AI USE INFORMATION COLLECTED FOR? Any
            information collected by us is for a purpose in relation to your use
            of our Website or App, including for providing the services,
            processing, and delivering your orders. • Provide Services to you:
            We collect information to send you email about your registration,
            App Updates and Marketing Campaigns, perform tasks required to
            complete a transaction, provide customer support, or provide other
            types of customer relationship management and fulfillment. We may
            also use your information to optimize or improve our Services and
            operations, for example, training and quality assurance. •
            Fulfilling legal and compliance obligations: We collect information
            to fulfill our legal obligations. Examples of this may include
            satisfying regulatory screening requirements in connection with
            entity formations, responding to subpoenas and other legal requests
            for information, and maintaining records as required in our role as
            Registered Agent. We may also collect information to detect fraud or
            other suspicious activity. • Analytics: We collect information for
            use in performing various types of analytics. For example, we use
            the information to analyze how visitors interact with our Website or
            App, where visitors to our Website or App arrive from and exit to,
            pages visited, links clicked, text entered and mouse movements to
            understand site usage, to detect fraud and potential threats and
            improve our services. • To communicate with you: We collect your
            email address and phone number to (i) contact you regarding your use
            of our Website or App/Website or App, (ii) respond to your
            questions, complaints, and queries, and (iii) send you updates
            regarding our services or this Policy and other agreements; • For
            other purposes through your consent: We may also process your
            information for some other reasons which your consent will be sought
            before we process such information. You may also withdraw your
            consent at any time. Third-party Information Collection 1. When you
            use the Website or App, certain third parties may use automatic
            information collection technologies to collect information about you
            or your device. These third parties may include: • Advertisers, ad
            networks and ad servers. • Analytics companies. • Your mobile device
            manufacturer. • Your mobile service provider. <br />
            <br />
            2. These third parties may use tracking technologies to collect
            information about you when you use the Website or App. The
            information they collect may be associated with your personal
            information or they may collect information, including personal
            information, about your online activities over time and across
            different Website or Apps, Website or Apps and other online services
            or Website or Apps. They may use this information to provide you
            with interest-based (behavioral) advertising or other targeted
            content. <br />
            <br />
            3. We do not control these third parties’ tracking technologies or
            how they may be used. If you have any questions about an
            advertisement or other targeted content, you should contact the
            responsible provider directly. 4. For information about how you can
            opt out of receiving targeted advertising from many providers.{" "}
            <br />
            <br /> 4. HOW PERSONAL DATA IS COLLECTED. 4.1 Automatic Information
            Collection and Tracking. When you access and use the Website or App,
            it may use technology to automatically collect: • Usage Details.
            When you access and use the Website or App, we may automatically
            collect certain details of your access to and use of the Website or
            App, including traffic data, location data, logs and other
            communication data and the resources that you access and use on or
            through the Website or App. 4.2 If you do not want us to collect
            this information, please do not create an account with us. <br />
            <br />
            4.3 We may also use these technologies to collect information about
            your activities over time and across third-party Website or Apps,
            Website or Apps or other online services (behavioral tracking).{" "}
            <br />
            <br />
            4.4 Information Collection and Tracking Technologies. The
            technologies we use for automatic information collection may
            include: • Cookies (or mobile cookies). A cookie is a small file
            placed on your smartphone or device. It may be possible to refuse to
            accept mobile cookies by activating the setting on your browser.
            However, if you select this setting you may be unable to access
            certain parts of our Website or App. The cookies we use include
            “analytical” cookies. They allow us to recognize and count the
            number of visitors and to see how visitors move around the Website
            or App when they are using it. This helps us to improve the way our
            Website or App works, for example, by ensuring that users are
            finding what they are looking for easily. These cookies are used to
            collect information about how visitors use our Website or App. We
            use the information to compile reports on general user traffic,
            conversion statistics and to help us improve the site. The cookies
            collect information anonymously. Please note that our advertisers
            may also use cookies, over which we have no control. • Web Beacons.
            Pages of the Website or App and our e-mails may contain small
            electronic files known as web beacons (also referred to as clear
            gifs, pixel tags and single-pixel gifs) that permit the Company, for
            example, to count users who have visited those pages or opened an
            e-mail and for other related Website or App statistics (for example,
            recording the popularity of certain Website or App content and
            verifying system and server integrity). • Third Party Cookies. The
            Website or App may link to other Website or Apps owned and operated
            by certain trusted third parties (for example Facebook) who, for
            example, collect data that helps us to track conversions from
            Facebook ads. These other third-party Website or Apps may also use
            cookies or similar technologies in accordance with their own
            separate polices. Please note that our advertisers may also use
            cookies, over which we have no control. For privacy information
            relating to these other third-party Website or Apps, please consult
            their policies as Appropriate.
            <br />
            <br />
            5. RECIPENTS AND TRANSFERS TO THIRD COUNTRIES The Provider may
            appoint third parties – data processors to perform some of the
            personal data processing operations, however only for the purposes
            specified in this Policy, on a basis of written agreement and under
            statutory conditions. Otherwise the Provider does not transfer or
            disclose your Personal Data to any third parties unless you have
            consented otherwise under this Policy or initiated the disclosure or
            except as required by the applicable laws. Sharing your Personal
            Data with other users of Springbox Ai is completely under your
            control. For the transfer of personal data to the United Arab
            Emirates, there is no Commission decision on adequate protection and
            therefore the transfer is carried out on the basis of appropriate
            safeguards in the form of standard data protection contractual
            clauses as approved by the European Commission. A copy of your
            Personal data can be obtained from the data processors through the
            Provider. 6. HOW WE USE PERSONAL DATA AND LAWFUL BASES FOR
            PROCESSING
            <br />
            <br />
            5.1 Lawful Bases for Processing under the GDPR What are the lawful
            bases for processing personal data? The lawful bases for processing
            are set out in Article 6 of the GDPR. At least one of these must
            apply whenever we process personal data. We mainly use consent,
            contract, legal obligations and legitimate interests as the bases to
            process your personal data in accordance with this privacy policy. •
            Consent: the individual has given clear consent for us to process
            their personal data for a specific purpose. • Contract: the
            processing is necessary for a contract we have with the individual,
            or because they have asked us to take specific steps before entering
            into a contract. • Legal obligation: the processing is necessary for
            us to comply with the law (not including contractual obligations). •
            Legitimate interests: the processing is necessary for our legitimate
            interests or the legitimate interests of a third party unless there
            is a good reason to protect the individual’s personal data which
            overrides those legitimate interests.
            <br />
            <br />
            5.2 We use information that we collect about you or that you provide
            to us, including any personal information, to: • To set up and
            operate your Springbox Ai account (such processing is necessary for
            the performance of our contract with you). • To verify your identity
            and carry out security checks in order to set up your account (such
            processing is necessary for the performance of our contract with you
            and necessary for us to comply with a legal obligation). • Provide
            you with the Website or App and its contents, and any other
            information, products or services that you request from us (such
            processing is necessary for the performance of our contract with
            you). • Give you notices about your account, the account you create
            (such processing is necessary for the performance of our contract
            with you and necessary for us to comply with a legal obligation). •
            Ensure that content from our Website or App is presented in the most
            effective manner for you and for your computer or device for
            accessing the Website or App (such processing is necessary for the
            performance of our contract with you). • Carry out our obligations
            and enforce our rights arising from any contracts entered into
            between you and us, including for billing and collection (such
            processing is necessary for our legitimate interests). • To manage
            your account, including processing payments and providing
            notifications, should this service be introduced within the Website
            or App (such processing is necessary for the performance of our
            contract with you and is necessary for our legitimate interests (to
            process payments).
            <br />
            <br />
            5.3 The usage information we collect helps us to improve our Website
            or App and to deliver a better and more personalized experience by
            enabling us to: • Estimate our audience size and usage patterns
            (such processing is necessary for our legitimate interests (for
            running our services and to study how users use our Website or App).
            • Store information about your preferences, allowing us to customize
            our Website or App according to your individual interests (such
            processing is necessary for our legitimate interests (for running
            our services). • Speed up your searches (such processing is
            necessary for our legitimate interests (for running our services). •
            Recognize you when you use the Website or App (such processing is
            necessary for the performance of our contract). • To provide us with
            information about how the Website or App is running or whether there
            are any faults or issues with our products and services (such
            processing is necessary for our legitimate interests (for us to
            deliver a better service to you).
            <br />
            <br />
            5.4 We may also use your information to contact you about news,
            offers, notifications, surveys, information, goods and services that
            we think may be of interest to you (such processing is done with
            your consent and is necessary for our legitimate interests (to
            develop our products/services and grow our business)). We give you
            the option to consent to such emails when you sign up for the
            Website or App. If you do not want us to use your information in
            this way, please do not consent. You may adjust your user
            preferences in your account profile.
            <br />
            <br />
            5.5 We have implemented reasonable measures designed to secure your
            personal information from accidental loss and from unauthorized
            access, use, alteration and disclosure.
            <br />
            <br />
            5.6 The safety and security of your information also depends on you.
            Where we have given you (or where you have chosen) a password for
            access to certain parts of our Website or App, you are responsible
            for keeping this password confidential. We ask you not to share your
            password with anyone. We urge you to be careful about giving out
            information in public areas of the Website or App like message
            boards. The information you share in public areas may be viewed by
            any user of the Website or App.
            <br />
            <br />
            5.7 Unfortunately, the transmission of information via the internet
            and mobile platforms is not completely secure. Although we do our
            best to protect your personal information, we cannot guarantee the
            security of your personal information transmitted through our
            Website or App. Any transmission of personal information is at your
            own risk. We are not responsible for circumvention of any privacy
            settings or security measures we provide.
            <br />
            <br />
            5.8 If you suspect that there has been a breach of the security of
            your data you should contact us at: info@springbox.tech and include
            details of: • the nature of the breach; • the date of the breach;
            and • the full circumstances of the breach.
            <br />
            <br />
            5.9 On notification of such a breach we will investigate the matter
            and, where appropriate and required by law, notify the relevant Data
            Protection Regulator.
            <br />
            <br />
            7 INTERNATIONAL USE OF THE WEBSITE OR APP <br />
            <br />
            This Privacy Policy is intended to cover collection of information
            on or via the App or our Website or App from residents of the United
            Arab Emirates. If you are using the App or visiting our Website or
            App from outside the United Arab Emirates, please be aware that your
            information may be transferred to, stored, and processed in the
            United Arab Emirates where our servers are located and our central
            database is operated. The data protection and other laws of the
            United Arab Emirates and other countries might not be as
            comprehensive as those in your country. Please be assured that we
            seek to take reasonable steps to ensure that your privacy is
            protected. By using our services, you understand that your
            information may be transferred to our facilities and those third
            parties with whom we share it as described in this privacy policy.
            <br />
            <br />
            8 GOOGLE LOGIN <br />
            <br />
            You may also sign up for Springbox Ai with your personal Google
            account. In that case, you shall give Provider all the requested
            permissions to collect your Personal Data.
            <br />
            <br />
            9 FACEBOOK LOGIN <br />
            <br />
            You may also sign up for Springbox Ai with your personal Google
            account. In that case, you shall give Provider all the requested
            permissions to collect your Personal Data.
            <br />
            <br />
            10 SIGN IN WITH APPLE <br />
            <br /> You acknowledge and agree that if you have chosen to
            anonymize your user data as part of Sign In with Apple and later you
            decide to link your Springbox Ai account with your Facebook or
            Google account or you connect your bank account or other financial
            service to your Springbox Ai account, your anonymized data may get
            linked with information that directly identifies you.
            <br />
            <br />
            11 TECHNICAL DATA <br />
            <br />
            You acknowledge that Provider may collect and use technical data,
            including but not limited to information about your mobile device
            and system, Internet connection or your use of Springbox Ai
            application. The purpose of the collection of technical data is to
            ensure quality, increase your comfort and further analyze and
            develop Springbox Ai and related services. Technical data is
            anonymous and stored separately from the Personal Data, therefore it
            cannot be linked back to you in any way in order to identify you.
            <br />
            <br />
            12 HOW PERSONAL DATA IS SHARED AND WITH WHOM <br />
            <br />
            Springbox Ai does not sell or share your personal information to
            third parties but may share your information if need arises such as
            the following: • To a buyer or other successor in the event of a
            merger, divestiture, restructuring, reorganization, dissolution or
            other sale or transfer of some or all of the Company’s assets,
            whether as a going concern or as part of bankruptcy, liquidation or
            similar proceeding, in which personal information held by the
            Company about our Website or App users is among the assets
            transferred. • For any other purpose disclosed by us when you
            provide the information. • For the purposes of academic research. •
            For any other purpose with your consent. • To comply with any court
            order, law or legal process, including to respond to any government
            or regulatory request. • To enforce our rights arising from any
            contracts entered into between you and us. If we believe disclosure
            is necessary or appropriate to protect the rights, property, or
            safety of the Company, our customers or others. This includes
            exchanging information with other companies and organizations for
            the purposes of fraud protection and credit risk reduction.
            <br />
            <br />
            13 WHAT ARE YOUR PRIVACY RIGHTS? <br />
            <br />
            If you are resident in the European Economic Area and you believe we
            are unlawfully processing your personal information, you also have
            the right to complain to your local data protection supervisory
            authority. You can find their contact details here:
            http://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.
            If you have questions or comments about your privacy rights, you may
            email us at info@springbox.tech Account Information If you would at
            any time like to review or change the information in your account or
            terminate your account, you can: ■ Log into your account settings
            and update your user account. Upon your request to terminate your
            account, we will deactivate or delete your account and information
            from our active databases. However, some information may be retained
            in our files to prevent fraud, troubleshoot problems, assist with
            any investigations, enforce our Terms of Use and/or comply with
            legal requirements. Opting out of email marketing: You can
            unsubscribe from our marketing email list at any time by clicking on
            the unsubscribe link in the emails that we send or by contacting us
            using the details provided below. You will then be removed from the
            marketing email list – however, we will still need to send you
            service-related emails that are necessary for the administration and
            use of your account
            <br />
            <br />
            14 YOUR RIGHTS WITH RESPECT TO PERSONAL DATA. <br />
            <br /> • You can review and change your personal information by
            logging into the Website or App and visiting your account settings
            page. • You may also send us an e-mail at info@springbox.tech to
            request access to, correct or delete any personal information free
            of charge that you have provided to us. We cannot delete your
            personal information except by also deleting your user account. • We
            will respond to a request to access, correct or delete any data
            within 30 days of the date we receive the request. • We may not
            accommodate a request to change information if we believe the change
            would violate or breach any law or legal requirement or cause the
            information to be incorrect. • If, on your request, we refuse to
            amend, correct or delete your personal information, we will set out
            our reasons for not doing so and provide you with details of how you
            may complain about our refusal. • If you delete your User
            Contributions from the Website or App, copies of your User
            Contributions may remain viewable in cached and archived pages, or
            might have been copied or stored by other Website or App users.
            Proper access and use of information provided on the Website or App,
            including User Contributions, is governed by our Terms and
            Conditions.
            <br />
            <br />
            15 INTERNATIONAL TRANSFER OF INFORMATION The information we hold
            about you may, from time-to-time, be transferred or shared with
            Contractors and subsidiaries located outside the EEA and EU for the
            purposes described in this Policy. When we transfer information in
            this manner, your personal information will also be subject to the
            policies set out in this Policy and other safeguards provided by
            law.
            <br />
            <br />
            16 CHILDREN’S PRIVACY The Children’s Online Privacy Protection Act
            was created to protect children under the age of 13 from
            unsuspecting acts or practices in conjunction with collecting,
            using, and disclosing any information about them. Our Services are
            not intended for anyone under the age of 13. If you are under 13, do
            not use or provide any information on or through our Services. If we
            learn we have collected or received personal information from a
            child under 13 without verification of parental consent, we will
            delete that information. If you are a parent or guardian or
            otherwise believe we might have any information from or about a
            child under the age of 13, please contact us so that we can delete
            the child’s information. The Services will never knowingly accept,
            collect, maintain or use any information from a child under the age
            of 13. If a child whom we know to be under the age of 13 sends
            personal information to us online, we will only use that information
            to respond directly to that child or notify parents
            <br />
            <br />
            17 HOW LONG PERSONAL DATA IS KEPT • We will only retain personal
            data for as long as is reasonably necessary. • Upon deletion of an
            account, all personal data will be removed as soon as possible, and
            always within 90 days of the deletion.
            <br />
            <br />
            18 LEGAL BASIS FOR THE PROCESSING OF PERSONAL INFORMATION 1. We rely
            upon both consent and legitimate business interests for processing
            personal information. For example, we may process personal
            information with a User’s consent when they agree that we may place
            cookies on their devices, or that we can process information that
            they enter into a form on one of our sites.
            <br />
            <br />
            2. It is in our interest as a business to enhance the services that
            we provide which may include processing your information to enable
            us to: • Respond to User enquiries and to fulfil User requests •
            Send Customers or Potential Customers relevant marketing information
            and offers • Complete transactions • Provide customer service • Send
            administrative information • Personalize Users’ experiences with our
            sites • Do so where we are required to by law
            <br />
            <br />
            3. We may also process personal information when it is in our or a
            Users’ legitimate interests to do so and when these interests are
            not overridden by an individual’s data protection rights.
            <br />
            <br />
            19 DATA SECURITY We have implemented multiple security measures
            designed to secure your personal information from accidental loss
            and unauthorized access, use, alteration, and disclosure. All
            information you provide to us is stored on our secure servers behind
            firewalls. Any payment transactions will be encrypted using SSL
            technology. The safety and security of your information also depend
            on you. Where we have given you (or where you have chosen) a
            password for access to certain parts of our Website or App, you are
            responsible for keeping this password confidential. We ask you not
            to share your password with anyone. Unfortunately, the transmission
            of information via the internet is not completely secure. Although
            we do our best to protect your personal information, we cannot
            guarantee the security of your personal information transmitted to
            our Website or App. Any transmission of personal information is at
            your own risk. We are not responsible for circumvention of any
            privacy settings or security measures contained on the Website or
            App.
            <br />
            <br />
            20 DO NOT TRACK DISCLOSURE Third parties such as advertising
            networks, analytics providers, and widget providers may collect
            information about your online activities over time and across
            different Website or Apps when you access or use our Service.
            <br />
            <br />
            21 CHANGES TO OUR PRIVACY POLICY It is our policy to post any
            changes we make to our privacy policy on this page with a notice
            that the privacy policy has been updated on the Website or App home
            page. If we make material changes to how we treat our users'
            personal information, we will notify you by e-mail to the primary
            e-mail address specified in your account and/or through a notice on
            the Website or App home page. The date the privacy policy was last
            revised is identified at the top of the page. You are responsible
            for ensuring we have an up-to-date active and deliverable e-mail
            address for you, and for periodically visiting our Website or App
            and this privacy policy to check for any changes.
          </p>
        </div>
      </div>
    </Layout>
  );
}
