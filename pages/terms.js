import React, { useState } from "react";
import { useEffect } from "react";

import Head from "next/head";
import styles from "../styles/pricing.module.css";
import ColorGuide from "../styles/colorGuide";

import useWindowSize from "../hooks/useWindowSize";

import Layout from "../components/layout";


export default function Subscribe() {

  useEffect(() => {
        if (typeof window !== 'undefined'){
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({ page_url: document.location.protocol + '//' + document.location.hostname + document.location.pathname + document.location.search,
      event:'page_view' });
    }
    
  }, []);

  const { width: responsiveWidth, height: responsiveHeight } = useWindowSize();
  const [currentFAQIndex, setCurrentFAQIndex] = useState(0);

  return (
    <Layout className={styles.container} pageTitle={"Terms & Conditions"}>
      <div
        className="full_bloc"
        style={{ backgroundColor: ColorGuide.mainWhite }}
      >
        <div className="container">
          <h1
            style={{
              color: ColorGuide.mainBlue,
              marginBottom: 40,
            }}
          >
            Terms & Conditions
          </h1>

          <p style={{ color: ColorGuide.mainBlue }}>
            The following Terms of Use (the "Terms of Use") govern your use of
            Springbox AI, (“Springbox AI” "Company" or "we" or "us") Website
            located at https://springbox.ai/terms (the "Site and App") and of
            the services provided by Springbox Ai (“Services”).
            <br />
            <br />
            1. BY USING THE SITE AND THE APP, YOU AGREE TO BE BOUND BY THESE
            TERMS OF USE. <br />
            <br />
            BY USING OR OTHERWISE ACCESSING THE WEBSITE/APP, POSTING OR
            DOWNLOADING CONTENT OR ANY OTHER INFORMATION TO OR FROM THE
            WEBSITE/APP OR MANIFESTING YOUR ASSENT TO THESE TERMS OF USE IN ANY
            OTHER MANNER, YOU HEREBY UNEQUIVOCALLY AND EXPRESSLY AGREE TO, AND
            SHALL BE SUBJECT TO, THESE TERMS OF USE. IF YOU DO NOT UNEQUIVOCALLY
            AGREE TO THESE TERMS OF USE, YOU MAY NOT USE OR OTHERWISE ACCESS THE
            WEBSITE/APP OR POST OR DOWNLOAD CONTENT OR ANY OTHER INFORMATION TO
            OR FROM THE WEBSITE/APP. YOUR SOLE REMEDY FOR DISSATISFACTION WITH
            THE SITE OR THE SERVICE OR ANY CONTENT IS TO STOP USING THE SITE.
            These Terms of Use May Change Springbox Ai reserves the right, at
            its discretion, to change, modify, add or remove portions of these
            Terms of Use at any time by posting such changes. Please check the
            Terms of Use page periodically for changes. The continued use of the
            Site and the Service following the posting of changes to these Terms
            of Use indicates that you accept those changes. If you have any
            questions or would like further clarification, please e-mail us at
            info@springbox.tech Additional Terms of Use May Apply The
            disclaimers, terms, and conditions on these pages are of general
            application and may be supplemented by additional policies,
            procedures, disclaimers, guidelines, rules, terms or conditions of
            specific application disclosed by Springbox Ai, including on any
            particular page of this Site or through the Services, or a
            registration process or other means. In the event of a conflict
            between these Terms of Use and any additional policies, procedures,
            disclaimers, guidelines, rules, terms or conditions of the specific
            application, the additional policies, procedures, disclaimers,
            guidelines, rules, terms or conditions of specific application shall
            control. Termination of Services for Non-compliance If you do not
            comply with the Terms of Use at any time, we reserve the right to
            terminate, limit, or otherwise alter your access to the Website/App.
            <br />
            <br />
            We May Discontinue or Alter Any Aspect of the Website/App. We may
            discontinue or alter any aspect of the Website/App, including, but
            not limited to, (i) restricting the time the Website/App is
            available, (ii) restricting the amount of use permitted, and (iii)
            restricting or terminating your right to use the Website/App, at our
            sole discretion and without prior notice or liability.
            <br />
            <br />
            2. AUTHORIZED USERS / REGISTRATION
            <br />
            <br />
            Definitions
            <br />
            <br />
            The terms “you,” “your” or “User(s)” refers to any individual
            accessing the Site or the Services for his/her own personal
            purposes, or on behalf of an entity or other person. In the event
            that you purport to be the agent of, represent, or otherwise act on
            behalf of an entity or any other person, references to “you,” “your”
            or “User(s)” shall include you individually and any such entity or
            person that you purport to represent, and you further represent and
            warrant that you are in fact an authorized representative of such
            entity or other person, that you have the authority to bind such
            entity or other person to these Terms of Use, and that your
            acceptance of these Terms of Use shall constitute acceptance on
            behalf of such entity or person. Authorization to Use this Site You
            hereby confirm to Springbox Ai that: (a) you have reached the age of
            13, (ii) are an emancipated minor under the laws of your
            jurisdiction of domicile or residence, (iii) have the consent of
            your parent or legal guardian, or (iv) otherwise have the power and
            authority to enter into and perform your obligations under these
            Terms of Use. Users between the ages of 13 and 18 must (i) review
            these Terms of Use with a parent or legal guardian to ensure the
            parent or legal guardian acknowledge and agree to these Terms of
            Use, and (ii) not access the Site if his or her parent or legal
            guardian doesn’t agree to these Terms of Use. Accounts For certain
            aspects of the Site, you may be asked to register an account. In the
            event you agree to register an account, you will select or receive a
            email and password upon providing registration information and
            successfully completing the registration process. You are solely
            responsible for maintaining the confidentiality of your account,
            email and password and for all activities and liabilities associated
            with or occurring under your account, email and password. You must
            notify us immediately of any unauthorized use of your account, email
            or password and any other breach of security, and (b) ensure that
            you exit from your account(s) at the end of each session. We cannot
            and will not be responsible for any loss or damage arising from your
            failure to comply with this requirement or as a result of use of
            your account, email or password, either with or without your
            knowledge. However, you could be held liable for losses incurred by
            us or another party due to someone else using your account, email or
            password. You may not transfer your account, email or password to
            another person, and you may not use anyone else's account, email or
            password at any time without the permission of the account holder.
            In cases where you have authorized or registered another individual,
            including a minor, to use your account, you are fully responsible
            for (i) the online conduct of such user; (ii) controlling the user's
            access to and use of the Services; and (iii) the consequences of any
            misuse. In consideration of your use of the Site, you agree to (a)
            provide true, accurate, current and complete information about
            yourself as prompted by the Service's registration form and to
            maintain and promptly update such information to keep it true,
            accurate, current and complete. If you provide any information that
            is untrue, inaccurate, not current or incomplete, or if we believe
            that such information is untrue, inaccurate, not current or
            incomplete, we reserve the right to suspend or terminate your
            account(s) and refuse any and all current or future use of the
            Website/App, or any portion thereof.
            <br />
            <br />
            3. SERVICES <br />
            <br />
            Springbox AI is the proprietary platform designed to assist you in
            your daily trading plan with live AI-powered signals. This means
            that whether you trade in stocks, forex, commodities or bonds, we
            bring clarity and real-time insight to your potential choices.
            Springbox Ai Website/Apps and domains, including
            https://springbox.ai/terms, and all of the webpages, Apps,
            subdomains, country-level domain variants and subparts of those
            Website/Apps (collectively, our “Site”), all of the services
            available on or through the Site or otherwise provided by us
            (including our application programming interfaces), and all of our
            free mobile applications (collectively, the “Applications”) are
            offered, maintained and provided by Springbox Ai. We refer to all of
            these as our “Services.”
            <br />
            <br />
            4. FEES AND BILLING
            <br />
            <br />
            Springbox Ai charge a monthly subscription fee to use the
            Website/App services and content. You are responsible to Springbox
            Ai for any fees applicable to services you choose. You authorize
            Springbox Ai (or its partners), or its designated payment processor,
            to automatically charge your specified credit card, debit card or
            other payment method for such fees as provided through the
            registration process. Unless otherwise specified, all fees are in
            United States dollars, and all charges will be made in United States
            dollars. Except as required by law, all fees are nonrefundable,
            including, without limitation. Payment may not be canceled by the
            user, except as required by law. However, Springbox Ai (or its
            partners) reserves the right to refuse or terminate any payment at
            any time in its sole discretion. You understand and agree that if
            you authorize a payment transaction with your credit card, debit
            card, or other payment method, but your charge is rejected for any
            reason, there may be a hold on your use of the Website/App services
            for several days. Subscription Cancellation Your preferred payment
            method will be automatically charged after your 14 days trial for
            your monthly or semi-annual subscription fees unless you manage your
            subscription by turning off the auto renewal feature through your
            iTunes account.
            <br />
            <br />
            5. RESTRICTIONS ON USE OF SITE CONTENT
            <br />
            <br />
            All content and materials contained in this Site or made available
            through the Service (“Content”), are protected by the United Arab
            Emirates and international trademark and copyright laws, are owned
            or controlled by Springbox Ai, and must only be used for certain
            approved purposes as established by Springbox Ai. You may only view
            or download Content from this Site for your use or as otherwise
            expressly authorized by Springbox Ai. Limited License Springbox Ai
            hereby grants you a limited, nonexclusive, nonassignable,
            nontransferable license to access and use the Site solely for your
            own personal, non-commercial purposes, subject to your agreement to,
            compliance with and satisfaction of these Terms of Use. All rights
            not otherwise expressly granted by these Terms of Use are reserved
            by Springbox Ai. If you do not comply with the Terms of Use at any
            time, Springbox Ai reserves the right to revoke the aforementioned
            license(s), limit your access to the Site, or restrict your ability
            to post or download Content, which may include the ordering of
            products and services. No Reproduction / Distribution The
            reproduction, duplication, distribution (including by way of e-mail,
            facsimile or other electronic means), publication, modification,
            copying or transmission of material available on or through this
            Site or the App is STRICTLY PROHIBITED without the prior written
            consent of Springbox Ai or unless expressly permitted by this Site
            or the App. This includes, without limitation, any application,
            text, graphics, logos, photographs, audio or video material or
            stills from audiovisual material available on this Site by Springbox
            Ai. The copying posting, linking, or another use of Content from
            this Site or the App on any other Website/App or networked computer
            environment is similarly prohibited. Requests for permission to
            reproduce or distribute materials found on this Site or the Service
            can be made by contacting Springbox Ai in writing at the address
            listed below. Trademarks The trademarks, logos, service marks, and
            trade names (collectively the "Trademarks") of Springbox Ai or any
            third party displayed on the Site or content available through the
            Site may not be used unless authorized by the trademark owner. All
            Trademarks not owned by us that appear on the Site or on or through
            the Site's services, if any, are the property of their respective
            owners. Nothing contained on the Site should be construed as
            granting, by implication, estoppel, or otherwise, any license or
            right to use any Trademark displayed on the Site without our written
            permission or that of the third-party rights holder. Your misuse of
            the Trademarks displayed on the Site is strictly prohibited.
            Third-Party Rights Trademarks, service marks, logos, and icons owned
            by third parties are the property of those respective third parties.
            Springbox Ai and affiliates do not warrant or represent that your
            use of the Content will not infringe the rights of third parties.
            <br />
            <br />
            6. LICENSE AND OWNERSHIP
            <br />
            <br />
            Springbox Ai application and all rights therein, including
            intellectual property rights, shall remain Springbox Ai's property
            or the property of its licensors. Nothing in the Terms shall be
            construed to grant you any rights, except for the limited license
            granted below. Subject to the Terms, Springbox Ai grants you a
            limited, non-exclusive, non-transferrable, non-sublicensable
            license, to access and use Springbox Ai and its Paid features
            purchased pursuant to the Terms on any same-platform device (i.e.
            iOS device) that you own and control. The license is granted solely
            for your personal, non-commercial use. Therefore, you may not rent,
            lease, sell, transfer, redistribute, or sublicense the Springbox Ai
            application. Third-party services or libraries included in Springbox
            Ai are licensed to you either under these Terms, or under the third
            party's license terms, if applicable. Based on your license, you may
            not access Springbox Ai with other means than the official
            application, mine or extract any data from Springbox Ai databases,
            modify, reverse engineer, hack, decode, decrypt, decompile,
            disassemble or create derivative works of Springbox Ai application
            or any part thereof and circumvent any technology used to protect
            the Paid features. You also may not remove, delete or obliterate any
            copyright notices, proprietary labels or private legends placed upon
            or found within the Springbox Ai application.
            <br />
            <br />
            7. LINKS
            <br />
            <br />
            You may be able to link from the Site to third party web sites and
            third-party web sites may link to the Site ("Linked Sites"). You
            acknowledge and agree that we have no responsibility for the
            content, products, services, advertising, or other materials that
            may be provided by or through Linked Sites, even if they are owned
            or run by affiliates of ours. Links to Linked Sites do not
            constitute an endorsement or sponsorship by us of such Website/Apps
            or the information, content, products, services, advertising, code
            or other materials presented on or through such Website/Apps. Any
            reliance on the contents of a third-party web site is done at your
            own risk and you assume all responsibilities and consequences
            resulting from such reliance.
            <br />
            <br />
            8. INTELLECTUAL PROPERTY
            <br />
            <br />
            The content available on the Website/App (“Website/App Content”),
            including the logos and trademarks (“Marks”), are licensed to
            Springbox Ai and are subject copyright, trademark, and other
            intellectual property rights under the United Arab Emirates (“UAE”),
            foreign laws, and international conventions. The Website/App Content
            includes, but are not limited to, the source codes, database,
            Website/App design, functionalities, videos, audios, texts, images,
            graphics, and items available on the Website/App. The logos,
            designs, page headers, names, script, graphic, button icons and
            footers are registered trademarks or common law trade dress of
            Springbox Ai in the US and other parts of the world. The Springbox
            Ai trademarks and trade dress may not be used in combination with
            any other products, services, or goods in a manner that is likely to
            confuse. You do not also have our permission to copy, imitate, or
            use it without getting written approval from us. Springbox Ai
            provides the Website/App Content to users on “AS IS” basis for
            information and personal use purposes. Users may not copy,
            reproduce, distribute, advertise, transmit, license, sell, or
            exploit it for any other purposes without seeking written consent
            from us.
            <br />
            <br />
            9. PROHIBITED USER CONDUCT
            <br />
            <br />
            We may investigate and/or terminate your account if you misuse the
            Site or behave in any way that we regard as inappropriate or
            unlawful. You agree that, while using the Site and the various
            services and features offered on or through the Site, you shall not:
            (a) impersonate any person or entity or misrepresent your
            affiliation with any other person or entity; (b) insert your own or
            a third party's advertising, branding or other promotional content
            into any of the Site's content, materials or services; or (c) gain
            or attempt to gain unauthorized access to other computer systems
            through the Site. You agree to not: (i) engage in spidering, "screen
            scraping," "database scraping," harvesting of e-mail addresses,
            wireless addresses or other contact or personal information, or any
            other automatic means of accessing, logging-in or registering on the
            Site or for any services or features offered on or through the Site,
            or obtaining lists of users or obtaining or accessing other
            information or features on, from or through the Site or the App
            offered on or through the Site, including, without limitation, any
            information residing on any server or database connected to the Site
            or any services offered on or through the Site; (ii) use the Site or
            the App made available on or through the Site in any manner with the
            intent to interrupt, damage, disable, overburden, or impair the Site
            or such services, including, without limitation, sending mass
            unsolicited messages or "flooding" servers with requests; or (iii)
            use the Site or the Site's services in violation of any applicable
            law or the legal rights of any third party. You further agree that
            you may not attempt (or encourage or support anyone else's attempt)
            to circumvent, reverse engineer, decrypt, or otherwise alter or
            interfere with the Site or the Site's services, or any content
            thereof, or make unauthorized use thereof. You shall not (nor cause
            any third party to) use the Site or the App to perform any illegal
            or immoral activities (including without limitation defaming,
            abusing, harassing, stalking, threatening, or otherwise violating
            the legal rights - such as rights of privacy of others) or any of
            the following types of activities, without limitation: (a)
            disseminating any unlawful, harassing, libelous, abusive,
            threatening, harmful, vulgar, pornographic, obscene or otherwise
            objectionable material; (b) transmitting information that violates
            any applicable federal, state, or local laws, rules or regulations,
            including any governmental agency guidelines, policies or
            procedures, or that infringes any patent, trademark, trade secret,
            copyright or other intellectual property or proprietary rights of
            any party; (c) conducting any activity that would constitute fraud;
            (d) transmitting any material that contains software viruses, trojan
            horses, worms, time bombs, cancelbots, or any other computer code,
            files, or programs which may interrupt, destroy, or limit the
            functionality of any computer software or hardware or
            telecommunications equipment; (e) use the Site or the App to gain
            competitive intelligence about Springbox Ai , the Site, or any
            product or service offered via the Site or to otherwise compete with
            Springbox Ai .
            <br />
            <br />
            10. MAINTENANCE AND SUPPORT
            <br />
            <br />
            Springbox Ai is subject to a continuous development and Springbox Ai
            reserves the right, at its sole discretion, to update the Springbox
            Ai application, change the nature of Springbox Ai or modify or
            discontinue some of the features without prior notice to you. You
            acknowledge that Springbox Ai has no obligation to maintain or
            update Springbox Ai. Springbox Ai does not guarantee an
            uninterrupted provision of the services. Springbox Ai or integrated
            third-party services may be temporary unavailable due to the
            maintenance, certain technical difficulties, or other events that
            are beyond Springbox Ai's control. If you have some questions,
            problems or suggestions, you can reach the Springbox Ai via contacts
            provided hereafter. However, you acknowledge that the support to
            nonpaying users of Springbox Ai is limited due to the limited
            capacity of the Springbox Ai.
            <br />
            <br />
            11. INDEMNIFICATION
            <br />
            <br />
            You agree to defend, indemnify and hold Company, its affiliates, and
            its and their directors, officers, employees and agents harmless
            from all claims, liabilities, costs, and expenses, including
            reasonable attorneys' fees, arising in any way from any content or
            other material you place on the Site or your breach or violation of
            the law or these Terms of Use. Company reserves the right, at its
            own expense, to assume the exclusive defense and control of any
            matter otherwise subject to indemnification by you, and in such
            case, you agree to cooperate with the Company's defense of such
            claim.
            <br />
            <br />
            12. MOBILE APPLICATION LICENSE
            <br />
            <br />
            Use License If you access the Application via a mobile application,
            then we grant you a revocable, non-exclusive, non-transferable,
            limited right to install and use the mobile application on wireless
            electronic devices owned or controlled by you, and to access and use
            the mobile application on such devices strictly in accordance with
            the Terms of Use of this mobile application license contained in
            these Terms of Use. You shall not: (1) decompile, reverse engineer,
            disassemble, attempt to derive the source code of, or decrypt the
            application; (2) make any modification, adaptation, improvement,
            enhancement, translation, or derivative work from the application;
            (3) violate any applicable laws, rules, or regulations in connection
            with your access or use of the application; (4) remove, alter, or
            obscure any proprietary notice (including any notice of copyright or
            trademark) posted by us or the licensors of the application; (5) use
            the application for any revenue generating endeavor, commercial
            enterprise, or other purpose for which it is not designed or
            intended; (6) make the application available over a network or other
            environment permitting access or use by multiple devices or users at
            the same time; (7) use the application for creating a product,
            service, or software that is, directly or indirectly, competitive
            with or in any way a substitute for the application; (8) use the
            application to send automated queries to any website or to send any
            unsolicited commercial e-mail; or (9) use any proprietary
            information or any of our interfaces or our other intellectual
            property in the design, development, manufacture, licensing, or
            distribution of any applications, accessories, or devices for use
            with the application.
            <br />
            <br />
            Apple and Android Devices
            <br />
            <br />
            The following terms apply when you use a mobile application obtained
            from either the Apple Store or Google Play (each an “App
            Distributor”) to access the Application: (1) the license granted to
            you for our mobile application is limited to a non-transferable
            license to use the application on a device that utilizes the Apple
            iOS or Android operating systems, as applicable, and in accordance
            with the usage rules set forth in the applicable App Distributor’s
            Terms of Use; (2) we are responsible for providing any maintenance
            and support services with respect to the mobile application as
            specified in the Terms of Use of this mobile application license
            contained in these Terms of Use or as otherwise required under
            applicable law, and you acknowledge that each App Distributor has no
            obligation whatsoever to furnish any maintenance and support
            services with respect to the mobile application; (3) in the event of
            any failure of the mobile application to conform to any applicable
            warranty, you may notify the applicable App Distributor, and the App
            Distributor, in accordance with its terms and policies, may refund
            the purchase price, if any, paid for the mobile application, and to
            the maximum extent permitted by applicable law, the App Distributor
            will have no other warranty obligation whatsoever with respect to
            the mobile application; (4) you must comply with applicable
            third-party terms of agreement when using the mobile application,
            e.g., if you have a VoIP application, then you must not be in
            violation of their wireless data service agreement when using the
            mobile application; and (5) you acknowledge and agree that the App
            Distributors are third-party beneficiaries of the Terms of Use in
            this mobile application license contained in these Terms of Use, and
            that each App Distributor will have the right (and will be deemed to
            have accepted the right) to enforce the Terms of Use in this mobile
            application license contained in these Terms of Use against you as a
            third-party beneficiary thereof.
            <br />
            <br />
            13. NO WARRANTIES
            <br />
            <br />
            TO THE MAXIMUM EXTENT ALLOWED BY LAW, THE SITE, INCLUDING, WITHOUT
            LIMITATION, ALL SERVICES, PRODUCTS, CONTENT, FUNCTIONS, AND
            MATERIALS CONTAINED OR AVAILABLE THEREON, IS PROVIDED "AS IS," "AS
            AVAILABLE", WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
            INCLUDING, WITHOUT LIMITATION, ANY WARRANTY FOR INFORMATION, DATA,
            DATA PROCESSING SERVICES, UPTIME OR UNINTERRUPTED ACCESS,
            AVAILABILITY, ACCURACY, USEFULNESS, OR CONTENT OF INFORMATION, AND
            ANY WARRANTIES OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR
            FITNESS FOR A PARTICULAR PURPOSE. WE HEREBY DISCLAIM ANY AND ALL
            SUCH WARRANTIES, EXPRESS AND IMPLIED. COMPANY ALSO ASSUMES NO
            RESPONSIBILITY, AND SHALL NOT BE LIABLE FOR, ANY DAMAGES TO, OR
            VIRUSES THAT MAY INFECT, YOUR COMPUTER EQUIPMENT OR OTHER PROPERTY
            ON ACCOUNT OF YOUR ACCESS TO OR USE OF THE SITE OR YOUR DOWNLOADING
            OF ANY MATERIALS FROM THE SITE. IF YOU ARE DISSATISFIED WITH THE
            SITE, YOUR SOLE REMEDY IS TO DISCONTINUE USING THE SITE. FURTHER,
            SPRINGBOX AI DOES NOT WARRANT (A) THE ACCURACY, COMPLETENESS,
            CURRENCY, OR RELIABILITY OF ANY CONTENT, (B) THAT THE RESULTS
            OBTAINED FROM THE USE OF THE SITE OR THE APP OR CONTENT WILL BE
            ACCURATE OR RELIABLE, OR (C) THAT THE QUALITY OF THE SITE OR THE APP
            OR CONTENT WILL MEET YOUR EXPECTATIONS.
            <br />
            <br />
            14. LIMITATION ON LIABILITY
            <br />
            <br />
            IN NO EVENT, INCLUDING BUT NOT LIMITED TO NEGLIGENCE, SHALL COMPANY,
            ANY OF ITS AFFILIATES, OR ANY OF THEIR DIRECTORS, OFFICERS,
            EMPLOYEES, AGENTS OR CONTENT OR SERVICE SPRINGBOX AI (COLLECTIVELY,
            THE "PROTECTED ENTITIES") BE LIABLE FOR ANY DIRECT, INDIRECT,
            SPECIAL, INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE DAMAGES
            ARISING FROM, OR DIRECTLY OR INDIRECTLY RELATED TO, THE USE OF, OR
            THE INABILITY TO USE, THE SITE OR THE CONTENT, MATERIALS, AND
            FUNCTIONS RELATED THERETO, YOUR PROVISION OF INFORMATION VIA THE
            SITE, LOST BUSINESS OR LOST SALES, EVEN IF SUCH PROTECTED ENTITY HAS
            BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME JURISDICTIONS
            DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL
            OR CONSEQUENTIAL DAMAGES SO SOME OF THE ABOVE LIMITATIONS MAY NOT
            APPLY TO CERTAIN USERS TO THE EXTENT REQUIRED BY APPLICABLE LAW. IN
            NO EVENT SHALL THE PROTECTED ENTITIES BE LIABLE FOR OR IN CONNECTION
            WITH ANY CONTENT POSTED, TRANSMITTED, EXCHANGED OR RECEIVED BY OR ON
            BEHALF OF ANY USER OR OTHER PERSON ON OR THROUGH THE SITE. IN NO
            EVENT SHALL THE TOTAL AGGREGATE LIABILITY OF THE PROTECTED ENTITIES
            TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF ACTION (WHETHER IN
            CONTRACT OR TORT, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE OR
            OTHERWISE) ARISING FROM THE TERMS OF USE OR YOUR USE OF THE SITE
            EXCEED, IN THE AGGREGATE, THE AMOUNT, IF ANY, PAID BY YOU TO COMPANY
            FOR YOUR USE OF THE SITE AND/OR SERVICES.
            <br />
            <br />
            15. DISCLAIMER
            <br />
            <br />
            EVERY EFFORT HAS BEEN MADE TO ACCURATELY REPRESENT THIS PRODUCT AND
            IT’S POTENTIAL. EVEN THOUGH THIS INDUSTRY IS ONE OF THE FEW WHERE
            ONE CAN WRITE THEIR OWN CHECK IN TERMS OF EARNINGS, THERE IS NO
            GUARANTEE THAT YOU WILL EARN ANY MONEY USING THE TECHNIQUES AND
            IDEAS IN THESE MATERIALS. EXAMPLES IN THESE MATERIALS ARE NOT TO BE
            INTERPRETED AS A PROMISE OR GUARANTEE OF EARNINGS. EARNING POTENTIAL
            IS ENTIRELY DEPENDENT ON THE PERSON USING OUR PRODUCT, IDEAS AND
            TECHNIQUES. WE DO NOT PURPORT THIS AS A “GET RICH SCHEME.” ANY
            CLAIMS MADE OF ACTUAL EARNINGS OR EXAMPLES OF ACTUAL RESULTS CAN BE
            VERIFIED UPON REQUEST. YOUR LEVEL OF SUCCESS IN ATTAINING THE
            RESULTS CLAIMED IN OUR MATERIALS DEPENDS ON THE TIME YOU DEVOTE TO
            THE PROGRAM, IDEAS AND TECHNIQUES MENTIONED, YOUR FINANCES,
            KNOWLEDGE AND VARIOUS SKILLS. SINCE THESE FACTORS DIFFER ACCORDING
            TO INDIVIDUALS, WE CANNOT GUARANTEE YOUR SUCCESS OR INCOME LEVEL.
            NOR ARE WE RESPONSIBLE FOR ANY OF YOUR ACTIONS. It's your
            responsibility to monitor the account and close trades to prevent
            lose. You must be aware of the risks and be willing to accept them
            in order to invest in any markets. Don’t trade with money you can’t
            afford to lose. This is neither a solicitation nor an offer to
            Buy/Sell future. No representation is being made that any account
            will or is likely to achieve profits or losses similar to those
            discussed on this web site. The past performance of any trading
            system or methodology is not necessarily indicative of future
            results.
            <br />
            <br />
            16. TERMINATION
            <br />
            <br />
            In addition to any other method of termination or suspension
            provided for in these Terms of Use, Springbox Ai reserves the right
            to terminate, change, suspend or discontinue any aspect of the Site
            or the Site's services at any time. Further, you agree that
            Springbox Ai shall not be liable to you or any third-party for any
            termination or suspension of your access to the Site or any part
            thereof, removal of Content. Any such termination, suspension or
            cancellation shall not affect any right or relief to which Springbox
            Ai may be entitled at law or in equity. Upon any such termination,
            suspension or cancellation, you shall terminate all use of the Site,
            the Services, and any Content and you will not be entitled to any
            refund of any fees or other charges, if any, paid in connection with
            such use.
            <br />
            <br />
            17. SEVERABILITY
            <br />
            <br />
            If any provision under these Conditions is found to be invalid or
            unenforceable by a court of competent jurisdiction, such an invalid
            or unenforceable provision shall be severed and shall not affect the
            validity and enforceability of the other valid and enforceable
            provisions.
            <br />
            <br />
            18. GOVERNING LAW
            <br />
            <br />
            You agree that the Website/App does not give rise to personal
            jurisdiction over Springbox Ai, either specific or general, in
            jurisdictions other than the laws of the Emirates of Dubai. Any
            claim or dispute between you and Springbox Ai that arises in whole
            or in part from the Service will be decided exclusively by a court
            of competent jurisdiction located in Dubai. Those who do not choose
            to access the Springbox Ai do so at their initiative and are
            responsible for compliance with all applicable laws including any
            applicable local laws.
            <br />
            <br />
            19. COPYRIGHT NOTICES
            <br />
            <br />
            If you believe that your work has been copied and posted on the Site
            in a way that constitutes copyright infringement, please provide our
            Copyright Agent with the following information: • an electronic or
            physical signature of the person authorized to act on behalf of the
            owner of the copyright interest; • a description of the copyrighted
            work that you claim has been infringed; • a description of where the
            material that you claim is infringing is located on the Website/App
            (and such description must be reasonably sufficient to enable
            Company to find the alleged infringing material); • your address,
            telephone number, and email address; • a written statement by you
            that you have a good faith belief that the disputed use is not
            authorized by the copyright owner, its agent, or the law; and • a
            statement by you, made under penalty of perjury, that the above
            information in your notice is accurate and that you are the
            copyright owner or authorized to act on the copyright owner's
            behalf. • Notice of claims of copyright infringement should be
            provided to Company’s Copyright Agent at info@springbox.tech
            <br />
            <br />
            20. CONTACT US
            <br />
            <br />
            For further information or to contact us with questions, concerns,
            or comments, you may email us at info@springbox.tech or visit our
            Chat support. Although we will, in most circumstances, be able to
            receive your email or other information provided through the Site,
            we do not guarantee that it will receive all such email or other
            information timely and accurately. We shall not be legally obligated
            to read, act on, or respond to any such email or other information.
          </p>
        </div>
      </div>
    </Layout>
  );
}
