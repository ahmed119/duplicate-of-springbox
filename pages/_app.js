import { useEffect } from "react";
import dynamic from "next/dynamic";
import routter from "next/router"
import TagManager from "react-gtm-module"

import "../styles/global.css";
import "react-multi-carousel/lib/styles.css";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


const tagManagerArgs = {
  gtmId: 'GTM-NCW62KD',
}


export default function App({ Component, pageProps }) {



  useEffect(() => {
    TagManager.initialize(tagManagerArgs)

        
    /*window.$crisp = [];
    window.CRISP_WEBSITE_ID = "c188c869-57bb-482e-9e95-6d8df07dec18";

    (function () {
      var d = document;
      var s = d.createElement("script");

      s.src = "https://client.crisp.chat/l.js";
      s.async = 1;
      d.getElementsByTagName("head")[0].appendChild(s);
    })();*/
  }, []);

  return (
    
    <>
      <Component {...pageProps} />{" "}
      <style jsx global>{`
        @font-face {
          font-family: "TitlingGothicFB Standard";
          src: url("/fonts/TitlingGothicFB-Standard.otf");
          font-weight: bold;
          font-style: normal;
          font-display: swap;
        }
        @font-face {
          font-family: "TitlingGothicFB Bold";
          src: url("/fonts/TitlingGothicFB-Standard.otf");
        }

        @font-face {
          font-family: "TitlingGothicFB Normal";
          src: url("/fonts/TitlingGothicFB-Regular.otf");
        }

        @font-face {
          font-family: "WorkSans Bold";
          src: url("/fonts/WorkSans-Medium.ttf");
        }

        @font-face {
          font-family: "WorkSans Normal";
          src: url("/fonts/WorkSans-Regular.ttf");
        }
      `}</style>
    </>
  );
}

