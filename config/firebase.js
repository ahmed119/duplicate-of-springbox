import firebase from "firebase/app";
import "firebase/database";

const prodCredentials = {
  apiKey: "AIzaSyDKhihRxgXNZg8Zoyby2EweiyhVSKFTNO4",
  authDomain: "springbox-67517.firebaseapp.com",
  databaseURL: "https://springbox-67517.firebaseio.com",
  projectId: "springbox-67517",
  storageBucket: "springbox-67517.appspot.com",
  messagingSenderId: "296220239341",
  appId: "1:296220239341:web:3a4221f34cd7e1c34ef3e4",
  measurementId: "G-1SQ61XYP7N",
};

if (!firebase.apps.length) {
  firebase.initializeApp(prodCredentials);
}

export default firebase;
