const withVideos = require("next-videos");

module.exports = {
  experimental: {
    optimizeFonts: true,
    optimizeImages: true,
  },
};

module.exports = withVideos();
