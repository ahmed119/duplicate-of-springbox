export const formatNumber =
    (
        value, //numeric or string value
        prefixText, //add someting in the begining of result, exmaple $
        postfixText,  //add someting in the end of result, example %
        decimalPlaces, //specify decimal places after . symbol, example 123.123
        symbolPrefix //calculate if + symbol needed in result, example +123.123
    ) => {
        const regex = /(\d)(?=(\d{3})+(?!\d))/g;

        if (value) {
            const numeriVal = parseFloat(value.toString());
            if (numeriVal && numeriVal !== NaN) {
                if (!prefixText) {
                    prefixText = '';
                }

                if (!postfixText) {
                    postfixText = '';
                }

                if (symbolPrefix) {
                    if (numeriVal > 0) {
                        return prefixText + '+' + numeriVal.toFixed(decimalPlaces || 2).replace(regex, '$1,') + postfixText;
                    }
                    else {
                        return prefixText + numeriVal.toFixed(decimalPlaces || 2).replace(regex, '$1,') + postfixText;
                    }
                }
                else {
                    return prefixText + numeriVal.toFixed(decimalPlaces || 2).replace(regex, '$1,') + postfixText;
                }
            }
        }

        return '-';
    };

export const formatMarket = (value) => {
    if (value) {
        const str = value.toString();
        if (str.length > 9) {
            return formatNumber(str.slice(0, -9), '$', 'B', 0, false);
        }
        else if (str.length > 6) {
            return formatNumber(str.slice(0, -6), '$', 'M', 0, false);
        }
        else {
            return formatNumber(value, '$', null, 2, false);
        }
    }

    return '-';
}