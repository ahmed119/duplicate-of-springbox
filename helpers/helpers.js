export const downloadFile = async ({ fileLink, fileName }) => {
  const json = JSON.stringify(fileLink);
  const blob = new Blob([json], { type: "application/pdf" });
  const href = await URL.createObjectURL(blob); // Create a downloadable link
  const link = document.createElement("a");
  link.href = fileLink;
  link.download = fileName + ".pdf";
  document.body.appendChild(link); // This can any part of your website
  link.click();
  document.body.removeChild(link);
};
