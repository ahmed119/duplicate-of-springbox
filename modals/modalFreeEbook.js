import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";

import ModalContainer from "../components/modalContainer";
import InputMailDownload from "../components/inputMailDownload";

import ColorGuide from "../styles/colorGuide";

import { downloadFile } from "../helpers/helpers";

export default function ModalFreeEbook({
  isVisible = false,
  setIsVisible = () => {},
}) {
  const router = useRouter();

  const [cardNumber, setCardNumber] = useState(null);

  return (
    <ModalContainer
      isVisible={isVisible}
      setIsVisible={setIsVisible}
      containerStyle={{
        position: "relative",
        width: 900,
        padding: "5%",
      }}
    >
      <div className="containerRow" style={{}}>
        <img
          style={{
            width: "40%",
          }}
          src={isVisible.image}
          alt="Download Ebook"
        />

        <div>
          <h4 style={{}}>FREE EBOOKS</h4>
          <h2>{isVisible.title}</h2>
          <p style={{ marginBottom: 40 }}>
            AI is the way of the future. Download our ebook with some awesome
            tips on how to make extra cash using AI.
          </p>
          <InputMailDownload
            afterMailSent={() =>
              downloadFile({
                fileLink: isVisible.downloadLink,
                fileName: isVisible.fileName,
              })
            }
          />
        </div>
      </div>

      <img
        onClick={() => setIsVisible(false)}
        style={{
          cursor: "pointer",
          position: "absolute",
          top: 30,
          right: 30,
          width: 30,
          height: 30,
        }}
        src={"/UI/close.png"}
        alt="Download Ebook"
      />
    </ModalContainer>
  );
}
