export default [
  { title: "About us", route: "/story" },
  { title: "Thought leadership", route: "/thoughtLeadership" },
  { title: "Pricing", route: "/pricing" },
  { title: "FAQ", route: "/pricing#FAQ" },
  { title: "Home", route: "/", onlyMobile: true },
];
