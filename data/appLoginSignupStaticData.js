export default [
    {
        "title": "Welcome to springbox.ai",
        "body": "Where trading opportunities meet<br/>AI-powered algorithms.",
        "style": {
            "height": "350px",
            "width": "90%",
            "left": "0",
            "right": "0",
            "top": "25px",
            "position": "relative",
            "margin": "auto"
        }
    },
    {
        "title": "Markets Forecasting",
        "body": "<u><b>Stay in the know:</b></u> get clear AI trading signals in real time and never miss out<br/>on an opportunity. Receive instant notifications as soon as it’s time for you<br/>to make a decision, whether it means increasing positions, cutting or<br/>collecting your profits. Our updates, your call.",
        "squareImg": true,
        "style": {
            "height": "370px",
            "width": "90%",
            "left": "0",
            "right": "0",
            "top": "5px",
            "position": "relative",
            "margin": "auto"
        }        
    },
    {
        "title": "Smart Signals",
        "body": "<u><b>No more ups and downs:</b></u> keep your potential loss in check and your<br/>potential profits unlimited, thanks to our dynamic target profit module.<br/>Start working with market volatility and its maximum potential, instead<br/>of depending on them.",
        "squareImg": false,
        "style": {
            "height": "350px",
            "width": "90%",
            "left": "0",
            "right": "0",
            "top": "20px",
            "position": "relative",
            "margin": "auto"
        }        
    },
    {
        "title": "Market Screening",
        "body": "<u><b>Live in the now:</b></u> we simultaneously scan the highest market patterns<br/>profitability on Stocks, Indices, Currencies, Commodities and Bonds<br/>markets and rank the Top assets to buy and sell at all times. Get the<br/>best out of chart analysis, and leave out the subjectivity.",
        "squareImg": false,
        "style": {
            "height": "350px",
            "width": "90%",
            "left": "0",
            "right": "0",
            "top": "30px",
            "position": "relative",
            "margin": "auto"
        }        
    },
    {
        "title": "High Accuracy",
        "body": "<u><b>Know what to expect:</b></u> our signals deliver high accuracy bringing<br/>predictability to an ever changing market. Get the appropriate position<br/>sizing and take the guesswork out of your profits.",
        "squareImg": true,
        "style": {
            "height": "400px",
            "width": "90%",
            "left": "0",
            "right": "0",
            "top": "-40px",
            "position": "relative",
            "margin": "auto"
        }        
    },
];