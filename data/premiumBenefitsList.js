export default [
  "Unlimited Buy/Sell trading signals (SL/TP)",
  "Instant notifications on when to collect your profits, increase your position or cut your losses.",
  "Real time P/L & Quotes.",
  "2 Personalized Charts Analysis per week.",
];
