export default [
  { title: "Twitter", icon: "/social/twitterOrange.png", link: "#" },
  { title: "Facebook", icon: "/social/facebookOrange.png", link: "#" },
  { title: "LinkedIn", icon: "/social/linkedin.png", link: "#" },
  { title: "Discord", icon: "/social/discordOrange.png", link: "#" },
];
