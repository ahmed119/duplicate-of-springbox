const mainStrenghtList = [
  {
    icon: '/services/designIcon.png',
    title: 'Design',
    text:
      "Vous êtes un perfectionniste dans l'âme et désirez ce qu'il y a de meilleur et de plus beau pour votre projet ? Nous aussi."
  },
  {
    icon: '/services/mobileDevIcon.png',
    title: 'iOS/Android',
    text:
      'Un code source intégralement développé en France par des ingénieurs en quête permanente de performances optimales.'
  },
  {
    icon: '/services/digitalStrategyIcon.png',
    title: 'Stratégie',
    text:
      "D'expérience, nous savons ce qui fonctionne et comment sublimer votre concept, nous dépassons nos limites pour faire exploser votre croissance."
  },
  {
    icon: '/services/maintenanceIcon.png',
    title: 'Maintenance',
    text:
      "Concentrez-vous sur le développement de votre activité, nous nous occupons de faire en sorte que tout fonctionne à merveille pour que vous ayez l'esprit tranquille."
  }
]

const methodValuesList = [
  {
    title: 'Analyse',
    text:
      'Nous vous proposons une étude de votre marché, le meilleur business modèle pour votre projet et vous accompagnons intégralement dans les choix techniques.'
  },
  {
    title: 'Design',
    text:
      'Notre équipe de design en veille permanente se charge de concevoir pour vous une interface claire et intuitive qui saura définir votre marque pour les années à venir.'
  },
  {
    title: 'Intégration',
    text:
      "L'intégration consiste à transformer un design en une application statique respectant à la perfection la vision du designer. Cette première prise en main vous permet de visualiser réellement votre projet."
  },
  {
    title: 'Développement continu',
    text:
      'Vous avez un accès permanent aux derniers avancements sur votre projet depuis votre téléphone, vos retours sont pris en compte et traités dans la journée.'
  },
  {
    title: 'Garantie sans défauts',
    text:
      'Votre projet est garanti contre tout défaut ou panne pendant un an, nous intervenons immédiatement sans frais supplémentaires.'
  },
  {
    title: 'Propriété intellectuelle',
    text:
      "Votre projet vous appartient, nous partageons avec vous l'intégralité du code source et des fichiers ayant servi à la conception sans frais supplémentaires."
  }
]

export { mainStrenghtList, methodValuesList }
