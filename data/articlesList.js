import moment from "moment";
import ColorGuide from "../styles/colorGuide";

const renderArticleImage = ({ url, alt }) => {
  return (
    <>
      <img
        style={{
          width: "100%",
          marginBottom: 10,
          marginTop: 30,
        }}
        src={url}
        alt={alt}
      />
      <p style={{ color: ColorGuide.mainBlue, opacity: 0.5 }}>{alt}</p>

      <div className="separator" />
    </>
  );
};

const renderCitation = (citation) => {
  return (
    <div
      className="containerItem"
      style={{
        backgroundColor: ColorGuide.lightBlue,
        marginTop: 30,
        marginBottom: 30,
      }}
    >
      <p style={{ fontWeight: "600" }}>"{citation}"</p>
    </div>
  );
};

export default [
  {
    title: (
      <span>
        5 Ways in <span className="underlineOrange">Which AI</span> is Shaping
        the Gen Z Investment Space
      </span>
    ),
    thumbnail: "/articles/fiveWays/articleThumbnail.png",
    publicationDate: moment("08/08/2021", "DD/MM/YYYY").toDate(),
    readTime: 8,
    summaryList: [
      "Financial investment is a complex topic, and one which is influenced by many macro-environmental variables. Economic growth, technological developments, demographics, and governmental policies are all very capable of substantially influencing how, where and when money is invested on both institutional and individual scales.",
      "Indeed, two of the factors mentioned above are synergizing and beginning to play a role in the world of personal finance and investment. One of them, is the rapid emergence of Generation Z onto the financial investment scene. The other, is AI.",
    ],
    paragraphList: [
      {
        title: "Generation Z",
        content: () => (
          <>
            <p>
              Generation Z is comprised of those people born in the window
              period falling between the mid-to-late 1990s and the early 2010s.
              Whilst the bulk of these people are still at school and probably
              not active within the investment scene, the oldest in this bracket
              are in their early 20s and are either graduating from colleges and
              universities soon, or have already done so and have entered the
              workforce. Having started their working lives gives these folk an
              income which some, contrary to the stereotypical image of the
              frivolous and hard-partying young person, will be looking to save
              and invest. Increasing numbers of graduating Gen Z individuals
              will be entering the workforce in the coming years, making this
              demographic group an increasingly powerful and influential one in
              the financial investment arena as time goes on.
            </p>
          </>
        ),
      },
      {
        title: "Artificial Intelligence (AI)",
        content: () => (
          <>
            <p>
              Developing at a rapid pace on a parallel track to the Generation Z
              demographic is that of artificial intelligence (AI). As you may
              well have noticed, as a cultural and technological force, AI is
              currently everywhere and it is difficult to read anything online
              without the topic being mentioned at least once on a seemingly
              daily basis. There is obviously good reason for this however, as
              it is one of the most exciting technologies of our time, and
              contains great potential to transform various industries with its
              application. These two dynamic forces are now beginning to combine
              to influence the personal finance space – here are some of ways in
              which this is happening.
            </p>

            {renderArticleImage({
              url: "/articles/fiveWays/digitalNatives.png",
              alt:
                "Gen Z “digital natives” have never lived in a time where the internet wasn’t all-pervasive and easily accessible.",
            })}
          </>
        ),
      },
      {
        title: "1. Technology is a Major Component of Gen Z Life",
        content: () => (
          <>
            <p>
              Generation Z is the most technologically-savvy population cohort
              yet. Often dubbed ‘digital natives’, this is a population group
              which has never lived in a time where the internet wasn’t
              all-pervasive and easily accessible. Smartphones have been a
              ‘thing’ for the majority of their lives. And social media tends to
              play a major part in their interaction with their peers via
              platforms like Instagram, Snapchat and TikTok. With so much
              involvement in the tech which surrounds them, Generation Z expects
              it to be integrated into every facet of their lives, with personal
              finance and investment no exception.
            </p>

            {renderCitation(
              "Generation Z Investors are looking to collaborate with the tech in order to give them an edge and help them to become better investors."
            )}

            <p>
              Not only do they expect this integration, but they want it to make
              their lives easier and better, and to aid them in their dealings
              with their environment. The prospect of introducing AI into their
              personal investment strategies is thus not an alien or
              intimidating one as it may be for older investors. Conversely,
              Generation Z investors are looking to collaborate with the tech in
              order to give them an edge and help them to become better
              investors. Due to their circumstances Gen Z is skilled at rapidly
              establishing familiarity and trust with new technologies, and for
              them, introducing AI to their investing experience is not
              something to be confused or suspicious about, but rather something
              to be welcomed and celebrated.
            </p>
          </>
        ),
      },
      {
        title: "2. AI Can Facilitate Values Which Matter to Gen Z",
        content: () => (
          <>
            <p>
              Gen Z is inheriting a world with many pressing issues.
              Environmental degradation, climate change, rising inequality,
              political polarisation and the ever-growing influence of Big Tech
              are some of the many difficulties which this population cohort
              will have to face as they become young adults. Having grown up in
              a world with these troubles has shaped some of the values which
              they hold dear. And in a world where greed and exploitation has
              led to rising inequality, Gen Z holds the concepts of fairness and
              equality in high regard. Tech elites tend to have access to tools
              and technologies to which the less privileged do not, giving them
              financial advantages and further compounding inequality. The
              advent of AI however, has the potential to democratise
              advantageous knowledge previously only available to the select
              few. New investment apps such as Springbox AI are levelling the
              playing field by making complex investment algorithms utilised by
              big investment firms accessible to everyone. Crunching big data
              sets and applying sophisticated AI technology to them enables
              these apps to supply their users with information such as
              profitability patterns and advanced trading forecasts. Being armed
              with this type of information allows private individual investors
              to base their investment decisions in an informed, intelligent and
              cost-effective manner.
            </p>
          </>
        ),
      },
      {
        title: "3. Gen Z Wants Instant and On-Demand Availability",
        content: () => (
          <>
            <p>
              The modern world is a fast-paced one, with the ability to do a
              vast range of things just an internet connection, a keyboard and a
              mouse-click away. Generation Z has grown up in a world where the
              ability to obtain goods, experiences and information is available
              very quickly, and is accustomed to operating in an on-demand
              environment. Attention spans are becoming shorter and failure by
              service providers to meet low response times can result in them
              missing out on opportunities marketing and selling to Gen Z
              consumers. When it comes to the personal finance space, Generation
              Z investors have similar expectations regarding the availability
              of facts and data on which to base their investment decisions.
              Digital investment apps like Springbox AI cater to this
              requirement, providing a variety of up-to-the-minute prices,
              statistics and graphs so that their users are well-equipped to
              make rational, informed and timeous decisions, and take advantage
              of opportunities for which there may be a limited time frame.
            </p>
          </>
        ),
      },
      {
        title: "4. Games and Gamification is a Big Deal",
        content: () => (
          <>
            <p>
              In recent years playing video games or ‘gaming’ has moved from
              being a fringe activity played by young teenage boys, into
              mainstream pop culture. Video game publishers have become better
              known and one only needs to notice the way in which the
              PlayStation 5 has been repeatedly sold out since its launch in
              late 2020, to see how much demand exists for Sony’s latest
              console. Gamers no longer almost solely consist of teenage males
              but include adults of all genders, whilst big game software titles
              such as Resident Evil, Tomb Raider and Warcraft have been
              developed into mainstream movies, such is their popularity and
              cultural influence. Digital investment apps arguably have a
              gamification element to them too, making them appeal to Generation
              Z investors. A variety of interactive and highly visual elements
              such as real-time numbers, graphs, charts and historical
              comparisons are nested within these platforms, making them stand
              out and appeal to users who are used to fast, ever-changing
              information coming at them from a variety of angles and sources,
              and who want to continually improve their investment performance.
            </p>

            {renderArticleImage({
              url: "/articles/fiveWays/socialMediaPlatform.png",
              alt:
                "Sites such as Youtube and Reddit are trusted by the majority of Gen Z and millennial investors as sources for infomation on invesments. Source: Motley Fool.",
            })}
          </>
        ),
      },
      {
        title:
          "5. Community and Social Media Networks Play an Important Role in Investing",
        content: () => (
          <>
            <p>
              Humans are social creatures and due to the integration of
              technology with every part of their lives, many Gen Z individuals
              find some degree of fulfilment in this area via social media
              platforms. Digital micro-communities are an active space where Gen
              Z can connect with their peers, share information about common
              interests and learn from each other in comfortable and
              non-intimidating spaces. Online investing resource The Motley Fool
              recently conducted a survey regarding which tools and sources of
              information Gen Z and Millennial investors regularly use. Among
              their key findings were that social media is a main source of
              investing advice, with 91% of Gen Z respondents using social media
              for information on investing, more than any other source of
              information. As an example of the abovementioned
              micro-communities, The Motley Fool also found that Reddit is
              trusted by the majority of Gen Z and millennial investors, with
              61% of their respondents finding Reddit to be a trustworthy source
              of information on investing.1 Not only is new tech like AI making
              information and algorithms available to all, but it’s clear that
              investment expertise is also being democratised and shared amongst
              online peer-based communities, in contrast to the traditional ways
              in which it was held exclusively by professional investment
              companies.
            </p>
          </>
        ),
      },
      {
        title: "Summary",
        content: () => (
          <>
            <p>
              Artificial intelligence is an emerging technology which holds
              incredible potential for Generation Z. As this population cohort
              gets older, the capabilities of AI will only get more powerful,
              and the applications even more diverse. Even today however, it
              holds major appeal within the financial investment space, and
              particularly with Gen Z. By aligning with values which are
              important to this group of people such as fairness and equality,
              and combining it with the speed and accuracy of digital
              technology, AI makes a strong case for itself as an advantageous
              and beneficial tool for any Gen Z investor.{" "}
            </p>
          </>
        ),
      },
    ],
  },
];
