export const downloadLinkList = [
  {
    source: "/badges/stock-trading-app.png",
    link: "https://apps.apple.com/us/app/springbox-ai/id1533228624",
  },
  {
    source: "/badges/stock-investing-app.png",
    link: "https://play.google.com/store/apps/details?id=com.springbox",
  },
];
